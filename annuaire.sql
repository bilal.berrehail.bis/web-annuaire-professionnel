/*
Navicat MySQL Data Transfer

Source Server         : WampServer Local 
Source Server Version : 50714
Source Host           : 172.17.80.87:3306
Source Database       : annuaire

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2017-06-19 13:31:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for annuaire_data
-- ----------------------------
DROP TABLE IF EXISTS `annuaire_data`;
CREATE TABLE `annuaire_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) DEFAULT NULL,
  `nom` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `numero` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2119 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of annuaire_data
-- ----------------------------
INSERT INTO `annuaire_data` VALUES ('6', '1', 'Mr DOREMUS Dominique', '2296');
INSERT INTO `annuaire_data` VALUES ('7', '2', 'Mme SERRE Juliette', '2284');
INSERT INTO `annuaire_data` VALUES ('8', '2', 'Mme CAUSSY BRAYER Sylvie', '2109');
INSERT INTO `annuaire_data` VALUES ('9', '2', 'Mme POTAUFEUX Martine', '2108');
INSERT INTO `annuaire_data` VALUES ('10', '2', 'Melle GIAMOUKIS Adélaïde', '2134');
INSERT INTO `annuaire_data` VALUES ('11', '3', 'Mme GARDAN Hélène', '2289');
INSERT INTO `annuaire_data` VALUES ('12', '3', 'Mme HOPFNER Michèle', '2139');
INSERT INTO `annuaire_data` VALUES ('13', '3', 'Mme LEGRAND Karen / Mme JUMELLE Béatrice', '2110');
INSERT INTO `annuaire_data` VALUES ('15', '4', 'Mr LOISEAU Philippe', '2298');
INSERT INTO `annuaire_data` VALUES ('16', '4', 'Mr CONREUX Christopher', '2297');
INSERT INTO `annuaire_data` VALUES ('17', '4', 'Mr DAMIEN Bruno', '2167');
INSERT INTO `annuaire_data` VALUES ('18', '4', 'Mr COLLIN Johan', '2213');
INSERT INTO `annuaire_data` VALUES ('19', '4', 'Mr LAMBERT Ludovic', '2299');
INSERT INTO `annuaire_data` VALUES ('20', '4', 'Salle serveur', '2157');
INSERT INTO `annuaire_data` VALUES ('37', '10', 'Melle PARNISARI Cécile', '2164');
INSERT INTO `annuaire_data` VALUES ('22', '5', 'Mr BADET Michaeël', '2271');
INSERT INTO `annuaire_data` VALUES ('23', '5', 'Mme FOUGEROUSE Véronique', '2113');
INSERT INTO `annuaire_data` VALUES ('24', '5', 'Mme LACOUR Agnès', '2112');
INSERT INTO `annuaire_data` VALUES ('25', '5', 'Mme WATRIN Karine', '2165');
INSERT INTO `annuaire_data` VALUES ('26', '5', 'Mme HOCQUAUX PERROUD Aurore', '2274');
INSERT INTO `annuaire_data` VALUES ('27', '5', 'Melle CARPENT Ophélie', '2212');
INSERT INTO `annuaire_data` VALUES ('28', '6', 'Mme LASSAUGE Muriel', '2121');
INSERT INTO `annuaire_data` VALUES ('29', '6', 'Mr RODRIGUES José', '2161');
INSERT INTO `annuaire_data` VALUES ('30', '7', 'Melle PREVOT Morgane', '2290');
INSERT INTO `annuaire_data` VALUES ('31', '7', 'Mme ROBIN Nathalie', '2293');
INSERT INTO `annuaire_data` VALUES ('32', '7', 'Mme CANDUZZI Géraldine', '2291');
INSERT INTO `annuaire_data` VALUES ('33', '8', 'Mme CHONE-GAMET Brigitte', '2294');
INSERT INTO `annuaire_data` VALUES ('34', '8', 'Mme TAROT Christelle', '2138');
INSERT INTO `annuaire_data` VALUES ('35', '8', 'Mr MINIOU Simon', '2295');
INSERT INTO `annuaire_data` VALUES ('36', '9', 'Mme NEMBRINI Sabrina', '2103');
INSERT INTO `annuaire_data` VALUES ('38', '10', 'Melle DEBACKERE Sandrine', '2102');
INSERT INTO `annuaire_data` VALUES ('39', '11', 'Mr IDA-ALI Khalid', '2272');
INSERT INTO `annuaire_data` VALUES ('40', '11', 'Mme LEDEVEDEC Nathalie', '2285');
INSERT INTO `annuaire_data` VALUES ('41', '11', 'Melle MUNSTER Linda', '2292');
INSERT INTO `annuaire_data` VALUES ('42', '12', 'Mr BOUQUET Jean-Pierre', '2124');
INSERT INTO `annuaire_data` VALUES ('43', '12', 'Mr GUILLEMIN Alexandre', '2275');
INSERT INTO `annuaire_data` VALUES ('44', '12', 'Mme DELETANG Gina', '2133');
INSERT INTO `annuaire_data` VALUES ('45', '12', 'Mr RZEPKA Florent', '2149');
INSERT INTO `annuaire_data` VALUES ('46', '13', 'Melle GOLLES Stéphanie', '2287');
INSERT INTO `annuaire_data` VALUES ('47', '13', 'Mr RIVAL Pierre', '2187');
INSERT INTO `annuaire_data` VALUES ('48', '14', 'Mr DENIS Patrick', '2278');
INSERT INTO `annuaire_data` VALUES ('49', '14', 'Mme PELLIS Catherine', '2280');
INSERT INTO `annuaire_data` VALUES ('50', '14', 'Mme IVA Isabelle', '2280');
INSERT INTO `annuaire_data` VALUES ('51', '14', 'Mme BUISSON Stéphanie', '2135');
INSERT INTO `annuaire_data` VALUES ('52', '14', 'Melle GUERINOT Mélanie', '2127');
INSERT INTO `annuaire_data` VALUES ('53', '14', 'Mme GUILLEM Carine', '2130');
INSERT INTO `annuaire_data` VALUES ('54', '15', 'Mr LESSARD Julien', '2282');
INSERT INTO `annuaire_data` VALUES ('55', '15', 'Mr DERE Grégory', '2276');
INSERT INTO `annuaire_data` VALUES ('56', '16', 'Médiathèque Albert Camus', '03.26.72.33.40');
INSERT INTO `annuaire_data` VALUES ('57', '16', 'CCAS', '03.26.72.43.43');
INSERT INTO `annuaire_data` VALUES ('58', '16', 'Maison de la Petite Enfance', '03.26.62.18.51');
INSERT INTO `annuaire_data` VALUES ('59', '16', 'Réussite Educative', '03.26.62.24.19');
INSERT INTO `annuaire_data` VALUES ('60', '16', 'Dir. Développment Economique', '03.26.62.10.40 / 3040');
INSERT INTO `annuaire_data` VALUES ('61', '16', 'S.T. Communautaires', '03.26.72.19.81 / 4981');
INSERT INTO `annuaire_data` VALUES ('62', '16', 'S.T. Municipaux', '03.26.74.50.25 / 5000');
INSERT INTO `annuaire_data` VALUES ('63', '16', 'Bibliothèque Adulte', '03.26.74.19.27');
INSERT INTO `annuaire_data` VALUES ('64', '16', 'Bibliothèque Jeunesse', '03.26.72.35.76');
INSERT INTO `annuaire_data` VALUES ('65', '16', 'Médiathèque', '03.26.72.13.95');
INSERT INTO `annuaire_data` VALUES ('66', '16', 'Ecole municipal de Musique', '03.26.72.19.33');
INSERT INTO `annuaire_data` VALUES ('67', '16', 'Police Municipal', '03.26.62.20.21');
INSERT INTO `annuaire_data` VALUES ('68', '16', 'Lucien Herr', '03.26.62.10.07');
INSERT INTO `annuaire_data` VALUES ('69', '16', 'Orange Bleu', '03.26.41.00.10');
INSERT INTO `annuaire_data` VALUES ('70', '16', 'Office du Tourisme', '03.26.74.45.30');
INSERT INTO `annuaire_data` VALUES ('71', '16', 'Centre Médico Scolaire (CMS)', '03.26.74.24.83');
INSERT INTO `annuaire_data` VALUES ('72', '16', 'Mr BEVIERE Xavier', '03.26.72.56.74');
INSERT INTO `annuaire_data` VALUES ('73', '16', 'Déchetterie', '03.26.74.19.65');
INSERT INTO `annuaire_data` VALUES ('74', '17', 'Mr DESIRONT Thierry (élémentaire)', '03.26.74.23.05');
INSERT INTO `annuaire_data` VALUES ('75', '17', 'Mme LEQUESNE Martine (maternelle)', '03.26.74.22.66');
INSERT INTO `annuaire_data` VALUES ('76', '18', 'Mme LEMUT Peggy (élémentaire)', '03.26.72.36.36');
INSERT INTO `annuaire_data` VALUES ('77', '18', 'Mme PENOT Céline (maternelle)', '03.26.72.36.83');
INSERT INTO `annuaire_data` VALUES ('78', '19', 'Mme LAURENT Emilie (élémentaire)', '03.26.74.07.50');
INSERT INTO `annuaire_data` VALUES ('79', '19', 'Mme HAAG Maud (maternelle)', '03.26.74.06.99');
INSERT INTO `annuaire_data` VALUES ('80', '20', 'Mme GAZZOLI Françoise (élémentaire)', '03.26.72.28.56');
INSERT INTO `annuaire_data` VALUES ('81', '20', 'Mme WIRTZ Marie-France (maternelle)', '03.26.72.09.79');
INSERT INTO `annuaire_data` VALUES ('82', '21', 'Mr LALANDA Tomas (élémentaire)', '03.26.74.19.99');
INSERT INTO `annuaire_data` VALUES ('83', '21', 'Mme DANNOUX Ludivine (maternelle)', '03.26.74.17.28');
INSERT INTO `annuaire_data` VALUES ('94', '22', 'Mme PINOT Vanessa (élémentaire)', '03.26.72.37.54');
INSERT INTO `annuaire_data` VALUES ('95', '22', 'Mme MARCHAND Claire (maternelle)', '03.26.72.37.56');
INSERT INTO `annuaire_data` VALUES ('96', '23', 'COURDEMANGES - Mme LANDEE Sandra', '03.26.72.29.66');
INSERT INTO `annuaire_data` VALUES ('97', '23', 'HUIRON - Mme GRAPTON Magalie', '03.26.72.05.24');
INSERT INTO `annuaire_data` VALUES ('98', '23', 'LOISY - Mme DESCHAMPS Brigitte', '03.26.74.59.30');
INSERT INTO `annuaire_data` VALUES ('99', '23', 'MAROLLES - Mr CICOGNANI Thierry', '03.26.72.16.43');
INSERT INTO `annuaire_data` VALUES ('100', '23', 'LA CHAUSSEE SUR MARNE - Mme Veronica Daubry', '03.26.41.34.32');
INSERT INTO `annuaire_data` VALUES ('101', '23', 'FRIGNICOURT - Mme BARTOLOMEO Catherine', '03.26.74.15.88');
INSERT INTO `annuaire_data` VALUES ('102', '23', 'FRIGNICOURT Maternelle - Mme ASTIER Valérie', '03.26.74.69.97');
INSERT INTO `annuaire_data` VALUES ('103', '23', 'PRINGY - Mme COVIAUX Sabine', '03.26.72.77.79');
INSERT INTO `annuaire_data` VALUES ('104', '23', 'SAINT-OUEN - Mr CHAVEROU Eric', '03.26.72.26.35');
INSERT INTO `annuaire_data` VALUES ('105', '23', 'COUVROT - Mme MARCHAND Isabelle', '03.26.62.12.69');

-- ----------------------------
-- Table structure for categorie
-- ----------------------------
DROP TABLE IF EXISTS `categorie`;
CREATE TABLE `categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of categorie
-- ----------------------------
INSERT INTO `categorie` VALUES ('1', 'S. Municipaux');
INSERT INTO `categorie` VALUES ('2', 'S. Communautaires');
INSERT INTO `categorie` VALUES ('3', 'Ecoles');
INSERT INTO `categorie` VALUES ('4', 'Cité');
INSERT INTO `categorie` VALUES ('5', 'Mairie');

-- ----------------------------
-- Table structure for comptes
-- ----------------------------
DROP TABLE IF EXISTS `comptes`;
CREATE TABLE `comptes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_utilisateur` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `mot_de_passe` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of comptes
-- ----------------------------
INSERT INTO `comptes` VALUES ('1', 'admin', 'admin');
INSERT INTO `comptes` VALUES ('2', 'Bilal', 'berrehail');
INSERT INTO `comptes` VALUES ('3', 'Ryanb', 'bougueraira');

-- ----------------------------
-- Table structure for compteurs
-- ----------------------------
DROP TABLE IF EXISTS `compteurs`;
CREATE TABLE `compteurs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `vues` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=184 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of compteurs
-- ----------------------------
INSERT INTO `compteurs` VALUES ('167', 'Service : Dir. Développement Economique', '1');
INSERT INTO `compteurs` VALUES ('168', 'Service : Verne', '3');
INSERT INTO `compteurs` VALUES ('169', 'Service : Affaires Culturelles', '1');
INSERT INTO `compteurs` VALUES ('170', 'Service : Ressources Humaines', '3');
INSERT INTO `compteurs` VALUES ('171', 'Mairie CC', '3');
INSERT INTO `compteurs` VALUES ('172', 'Service : S.T. Communautaires', '5');
INSERT INTO `compteurs` VALUES ('173', 'Page d\'accueil', '5');
INSERT INTO `compteurs` VALUES ('174', 'Service : S.T. Municipaux', '1');
INSERT INTO `compteurs` VALUES ('175', 'Service : Paul Fort', '1');
INSERT INTO `compteurs` VALUES ('176', 'Service : Maison de la Petite Enfance', '2');
INSERT INTO `compteurs` VALUES ('177', 'Service : PRU', '2');
INSERT INTO `compteurs` VALUES ('178', 'Service : Jules', '2');
INSERT INTO `compteurs` VALUES ('179', 'Service : CCAS', '1');
INSERT INTO `compteurs` VALUES ('180', 'Service : Médiathèque Albert Camus', '4');
INSERT INTO `compteurs` VALUES ('181', 'Service : Centre Médico Scolaire (CMS)', '1');
INSERT INTO `compteurs` VALUES ('182', 'Service : Affaires générales', '3');
INSERT INTO `compteurs` VALUES ('183', 'Service : Déchetterie', '1');

-- ----------------------------
-- Table structure for logs_changements
-- ----------------------------
DROP TABLE IF EXISTS `logs_changements`;
CREATE TABLE `logs_changements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `date` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `changement` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=127 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of logs_changements
-- ----------------------------
INSERT INTO `logs_changements` VALUES ('19', 'admin', '14-06-2017 - 09:41', 'REUSSI : Suppression de Mme BELLINI Christine !');
INSERT INTO `logs_changements` VALUES ('20', 'admin', '14-06-2017 - 09:42', 'REUSSI : Ajout de \' Mme BELLINI Christine \' avec le numéro [ 69 ] !');
INSERT INTO `logs_changements` VALUES ('21', 'admin', '14-06-2017 - 09:42', 'REUSSI : Modification des informations de [ Mr PREDESCU-BERNARD Marc-Adrian => Telephone ] !');
INSERT INTO `logs_changements` VALUES ('22', 'admin', '14-06-2017 - 09:42', 'REUSSI : Modification du numéro de : Mme HUGNET Brigitte [ 2188 => 2189 ] !');
INSERT INTO `logs_changements` VALUES ('23', 'admin', '14-06-2017 - 09:43', 'REUSSI : Modification du numéro de : Mme REGNAULD Marie-Christine [ 2153 =>  ] !');
INSERT INTO `logs_changements` VALUES ('24', 'admin', '14-06-2017 - 09:44', 'REUSSI : Suppression de Mme REGNAULD Marie-Christine !');
INSERT INTO `logs_changements` VALUES ('25', 'admin', '14-06-2017 - 09:50', 'REUSSI : Modification du numéro de : Mme BELLINI Christine [ 69 => 5 ] !');
INSERT INTO `logs_changements` VALUES ('26', 'admin', '14-06-2017 - 09:50', 'REUSSI : Modification du numéro de : Mme BELLINI Christine [ 5 =>  ] !');
INSERT INTO `logs_changements` VALUES ('27', 'admin', '14-06-2017 - 09:52', 'REUSSI : Suppression de Mme BELLINI Christine !');
INSERT INTO `logs_changements` VALUES ('28', 'admin', '14-06-2017 - 09:52', 'REUSSI : Modification du numéro de : Mme ROUSSEL Nadège [ 2140 =>  ] !');
INSERT INTO `logs_changements` VALUES ('29', 'admin', '14-06-2017 - 09:53', 'REUSSI : Modification du numéro de : Mme HUGNET Brigitte [ 2189 =>  ] !');
INSERT INTO `logs_changements` VALUES ('30', 'admin', '14-06-2017 - 13:53', 'REUSSI : Modification des informations de [ Mme LACOINE Nathalie =>   ] !');
INSERT INTO `logs_changements` VALUES ('31', 'admin', '14-06-2017 - 13:53', 'REUSSI : Modification des informations de [   =>  j ] !');
INSERT INTO `logs_changements` VALUES ('32', 'admin', '14-06-2017 - 13:53', 'REUSSI : Modification des informations de [  j =>  yuijhk ] !');
INSERT INTO `logs_changements` VALUES ('33', 'admin', '14-06-2017 - 13:53', 'REUSSI : Suppression de  yuijhk !');
INSERT INTO `logs_changements` VALUES ('34', 'admin', '14-06-2017 - 15:49', 'REUSSI : Modification du numéro de : Telephone [ 2296 => 22965 ] !');
INSERT INTO `logs_changements` VALUES ('35', 'admin', '14-06-2017 - 15:49', 'REUSSI : Modification du numéro de : Mr DOREMUS Dominique [ 2143 => 22964 ] !');
INSERT INTO `logs_changements` VALUES ('36', 'admin', '14-06-2017 - 16:05', 'REUSSI : Ajout du service \' Pokemon \' dans la catégorie \' S. Municipaux \' !');
INSERT INTO `logs_changements` VALUES ('37', 'admin', '14-06-2017 - 16:06', 'REUSSI : Ajout du service \' rezfsd \' dans la catégorie \' S. Municipaux \' !');
INSERT INTO `logs_changements` VALUES ('38', 'admin', '14-06-2017 - 16:07', 'REUSSI : Ajout du service \' Pokemon \' dans la catégorie \' Ecoles \' !');
INSERT INTO `logs_changements` VALUES ('39', 'admin', '15-06-2017 - 10:24', 'REUSSI : Ajout du service \' Psychologie \' dans la catégorie \' Mairie \' !');
INSERT INTO `logs_changements` VALUES ('40', 'admin', '15-06-2017 - 10:26', 'REUSSI : Ajout de \' Mme DETER Grondé \' avec le numéro [ 06.25.26.45.78 ] !');
INSERT INTO `logs_changements` VALUES ('41', 'admin', '15-06-2017 - 10:27', 'REUSSI : Suppression de Mme DETER Grondé !');
INSERT INTO `logs_changements` VALUES ('42', 'admin', '15-06-2017 - 10:27', 'REUSSI : Ajout de \' Mme DETER Grondé \' avec le numéro [ 06.25.23.45.12 ] !');
INSERT INTO `logs_changements` VALUES ('43', 'admin', '15-06-2017 - 10:27', 'REUSSI : Ajout de \' dd \' avec le numéro [ 06.25.23.45.12 ] !');
INSERT INTO `logs_changements` VALUES ('44', 'admin', '15-06-2017 - 10:28', 'REUSSI : Suppression de Mme DETER Grondé !');
INSERT INTO `logs_changements` VALUES ('91', 'Bilal', '16-06-2017 - 10:43', 'REUSSI : Réinitialisation des compteurs !');
INSERT INTO `logs_changements` VALUES ('92', 'Bilal', '16-06-2017 - 10:43', 'REUSSI : Réinitialisation des logs de connexions !');
INSERT INTO `logs_changements` VALUES ('46', 'admin', '15-06-2017 - 10:49', 'REUSSI : Modification du numéro de : COUVROT - Mme MARCHAND Isabelle [ 03.26.62.12.68 => 03.26.62.12.69 ] !');
INSERT INTO `logs_changements` VALUES ('47', 'admin', '15-06-2017 - 10:49', 'REUSSI : Modification des informations de [ Arnaud MASSELOT => Arnaud Pokahantas ] !');
INSERT INTO `logs_changements` VALUES ('48', 'admin', '15-06-2017 - 12:13', 'REUSSI : Suppression du service \' Pokemon \'');
INSERT INTO `logs_changements` VALUES ('49', 'admin', '15-06-2017 - 12:13', 'REUSSI : Réinitialisation des logs de connexions !');
INSERT INTO `logs_changements` VALUES ('50', 'admin', '15-06-2017 - 12:15', 'REUSSI : Ajout de \' e \' avec le numéro [ erz ] !');
INSERT INTO `logs_changements` VALUES ('51', 'admin', '15-06-2017 - 12:16', 'REUSSI : Suppression de e !');
INSERT INTO `logs_changements` VALUES ('52', 'admin', '15-06-2017 - 12:17', 'REUSSI : Ajout de \' tt \' avec le numéro [ tt ] !');
INSERT INTO `logs_changements` VALUES ('53', 'admin', '15-06-2017 - 12:17', 'REUSSI : Suppression de tt !');
INSERT INTO `logs_changements` VALUES ('54', 'admin', '15-06-2017 - 12:17', 'REUSSI : Ajout de \' rze \' avec le numéro [ rez ] !');
INSERT INTO `logs_changements` VALUES ('55', 'admin', '15-06-2017 - 12:17', 'REUSSI : Suppression de rze !');
INSERT INTO `logs_changements` VALUES ('56', 'admin', '15-06-2017 - 12:17', 'REUSSI : Suppression de dd !');
INSERT INTO `logs_changements` VALUES ('57', 'admin', '15-06-2017 - 12:50', 'REUSSI : Ajout de la Mairie \' a \'');
INSERT INTO `logs_changements` VALUES ('58', 'admin', '15-06-2017 - 12:56', 'REUSSI : Ajout de la Mairie \' a \'');
INSERT INTO `logs_changements` VALUES ('59', 'admin', '15-06-2017 - 12:56', 'REUSSI : Suppression de la Mairie \' a \'');
INSERT INTO `logs_changements` VALUES ('60', 'admin', '15-06-2017 - 15:27', 'REUSSI : Suppression de la Mairie \' SOULANGES \'');
INSERT INTO `logs_changements` VALUES ('61', 'admin', '15-06-2017 - 15:28', 'REUSSI : Suppression de la Mairie \' SOMPUIS \'');
INSERT INTO `logs_changements` VALUES ('62', 'admin', '15-06-2017 - 15:28', 'REUSSI : Suppression de la Mairie \' HUMBAUVILLE \'');
INSERT INTO `logs_changements` VALUES ('63', 'admin', '15-06-2017 - 15:34', 'REUSSI : Suppression de la Mairie \' ABLANCOURT \'');
INSERT INTO `logs_changements` VALUES ('64', 'admin', '15-06-2017 - 15:40', 'REUSSI : Suppression de la Mairie \' BLACY \'');
INSERT INTO `logs_changements` VALUES ('65', 'admin', '15-06-2017 - 15:47', 'REUSSI : Ajout de la Mairie \' a \'');
INSERT INTO `logs_changements` VALUES ('66', 'admin', '15-06-2017 - 15:48', 'REUSSI : Suppression de la Mairie \' a \'');
INSERT INTO `logs_changements` VALUES ('67', 'Bilal', '15-06-2017 - 15:50', 'REUSSI : Modification des informations de [ Telephone => Telephones ] !');
INSERT INTO `logs_changements` VALUES ('68', 'Bilal', '15-06-2017 - 15:50', 'REUSSI : Modification du numéro de : Mme SERRE Juliette [ 2284 => 22847 ] !');
INSERT INTO `logs_changements` VALUES ('69', 'Bilal', '15-06-2017 - 15:50', 'REUSSI : Modification des informations de [ Mme SERRE Juliette => Mme SERRE Juliette4 ] !');
INSERT INTO `logs_changements` VALUES ('70', 'Bilal', '15-06-2017 - 15:50', 'REUSSI : Modification du numéro de : Mme CAUSSY BRAYER Sylvie [ 2109 => 21094 ] !');
INSERT INTO `logs_changements` VALUES ('71', 'Bilal', '15-06-2017 - 15:50', 'REUSSI : Modification des informations de [ Telephones => Telephones7 ] !');
INSERT INTO `logs_changements` VALUES ('72', 'Bilal', '15-06-2017 - 15:50', 'REUSSI : Modification du numéro de : Mr DOREMUS Dominique [ 22964 => 229647 ] !');
INSERT INTO `logs_changements` VALUES ('73', 'Bilal', '15-06-2017 - 15:50', 'REUSSI : Modification des informations de [ Mr DOREMUS Dominique => Mr DOREMUS Dominique7 ] !');
INSERT INTO `logs_changements` VALUES ('74', 'Bilal', '15-06-2017 - 15:50', 'REUSSI : Modification des informations de [ Mme SERRE Juliette4 => Mme SERRE Juliette47 ] !');
INSERT INTO `logs_changements` VALUES ('75', 'Bilal', '15-06-2017 - 15:50', 'REUSSI : Modification du numéro de : Mme CAUSSY BRAYER Sylvie [ 21094 => 210947 ] !');
INSERT INTO `logs_changements` VALUES ('76', 'Bilal', '15-06-2017 - 15:50', 'REUSSI : Modification des informations de [ Telephones7 => Telephones ] !');
INSERT INTO `logs_changements` VALUES ('77', 'Bilal', '15-06-2017 - 15:50', 'REUSSI : Modification des informations de [ Mr DOREMUS Dominique7 => Mr DOREMUS Dominique ] !');
INSERT INTO `logs_changements` VALUES ('78', 'Bilal', '15-06-2017 - 15:50', 'REUSSI : Modification des informations de [ Mme SERRE Juliette47 => Mme SERRE Juliette ] !');
INSERT INTO `logs_changements` VALUES ('79', 'Bilal', '15-06-2017 - 15:50', 'REUSSI : Modification du numéro de : Mme CAUSSY BRAYER Sylvie [ 210947 => 2109 ] !');
INSERT INTO `logs_changements` VALUES ('80', 'Bilal', '15-06-2017 - 15:51', 'REUSSI : Modification des informations de [ Mme GARDAN Hélène => Haleine de merde ] !');
INSERT INTO `logs_changements` VALUES ('81', 'Bilal', '15-06-2017 - 15:51', 'REUSSI : Modification du numéro de : Mr LOISEAU Philippe [ 2298 => 069 ] !');
INSERT INTO `logs_changements` VALUES ('82', 'Bilal', '15-06-2017 - 15:51', 'REUSSI : Modification des informations de [ Salle serveur => Salle de biscuit ] !');
INSERT INTO `logs_changements` VALUES ('83', 'Bilal', '15-06-2017 - 15:52', 'REUSSI : Ajout du service \' PokemonAdv \' dans la catégorie \' Mairie \' !');
INSERT INTO `logs_changements` VALUES ('84', 'Bilal', '15-06-2017 - 15:52', 'REUSSI : Suppression du service \' Psychologie \'');
INSERT INTO `logs_changements` VALUES ('85', 'Bilal', '15-06-2017 - 15:53', 'REUSSI : Suppression du service \' PokemonAdv \'');
INSERT INTO `logs_changements` VALUES ('86', 'Bilal', '15-06-2017 - 15:54', 'REUSSI : Réinitialisation des compteurs !');
INSERT INTO `logs_changements` VALUES ('87', 'admin', '15-06-2017 - 16:00', 'REUSSI : Suppression de la Mairie \' ARZILLIERES Neuville \'');
INSERT INTO `logs_changements` VALUES ('88', 'Bilal', '16-06-2017 - 09:26', 'REUSSI : Suppression de la Mairie \' AULNAY L\\\'AITRE \'');
INSERT INTO `logs_changements` VALUES ('89', 'Bilal', '16-06-2017 - 09:28', 'REUSSI : Suppression de la Mairie \' SOMSOIS \'');
INSERT INTO `logs_changements` VALUES ('90', 'Bilal', '16-06-2017 - 09:29', 'REUSSI : Ajout de la Mairie \' Saint-Dizier \'');
INSERT INTO `logs_changements` VALUES ('93', 'Bilal', '16-06-2017 - 10:57', 'REUSSI : Modification des informations de [ Telephones => Telephonese ] !');
INSERT INTO `logs_changements` VALUES ('94', 'Bilal', '16-06-2017 - 10:57', 'REUSSI : Modification des informations de [ Mr DOREMUS Dominique => Mr DOREMUS Dominiquee ] !');
INSERT INTO `logs_changements` VALUES ('95', 'Bilal', '16-06-2017 - 11:04', 'REUSSI : Ajout de la Mairie \' a \'');
INSERT INTO `logs_changements` VALUES ('96', 'admin', '16-06-2017 - 11:34', 'REUSSI : Modification du nom de la Mairie [  => Jacques FORTIN ]');
INSERT INTO `logs_changements` VALUES ('97', 'admin', '16-06-2017 - 11:37', 'REUSSI : Modification du nom de la Mairie [ Pok => Pokr ]');
INSERT INTO `logs_changements` VALUES ('98', 'admin', '16-06-2017 - 11:37', 'REUSSI : Modification du nom de la Mairie [ 13 rue de la Haute Fontaine => 13 ruee de la Haute Fontaine ]');
INSERT INTO `logs_changements` VALUES ('99', 'admin', '16-06-2017 - 11:38', 'REUSSI : Modification du nom de la Mairie [ 03.26.72.81.52 / 06.40.05.14.67 => 03.26.725.81.52 / 06.40.05.14.67 ]');
INSERT INTO `logs_changements` VALUES ('100', 'admin', '16-06-2017 - 11:39', 'REUSSI : Modification du nom de la Mairie [ 03.26fsd.72.24.55 / 06.75.22.43.72 => pokk ]');
INSERT INTO `logs_changements` VALUES ('101', 'admin', '16-06-2017 - 11:39', 'REUSSI : Modification du nom de la Mairie [ pokk => salut ]');
INSERT INTO `logs_changements` VALUES ('102', 'admin', '16-06-2017 - 11:39', 'REUSSI : Modification du nom de la Mairie [ 03.26.74.07.98 => pourquoi ]');
INSERT INTO `logs_changements` VALUES ('103', 'admin', '16-06-2017 - 11:39', 'REUSSI : Modification du nom de la Mairie [ 03.26.72.80.23 => num ]');
INSERT INTO `logs_changements` VALUES ('104', 'admin', '16-06-2017 - 11:41', 'REUSSI : Modification du téléphone de la Mairie (Pokr) [ num => nume ]');
INSERT INTO `logs_changements` VALUES ('105', 'admin', '16-06-2017 - 11:42', 'REUSSI : Modification de l\'E-mail de la Mairie (Pokr) [ mairie.blaise@wanadoo.fr / fortinjacques51@gmail.com => mairie.blaiese@wanadoo.fr / fortinjacques51@gmail.com ]');
INSERT INTO `logs_changements` VALUES ('106', 'admin', '16-06-2017 - 11:49', 'REUSSI : Modification du nom de la Mairie (454654rez) [ 454654 => 454654rez ]');
INSERT INTO `logs_changements` VALUES ('107', 'admin', '16-06-2017 - 11:49', 'REUSSI : Modification de l\'Adresse perso du Maire (Jacques FORTIN) [ 78 de l\'Europe => 78 de l\\\'Europefsdfds ]');
INSERT INTO `logs_changements` VALUES ('108', 'admin', '16-06-2017 - 11:49', 'REUSSI : Modification du portable du Maire (Jacques FORTIN) [ 03.26.7257881.52 / 06.40.05.14.67 => 03.26.7257881.52 rezf/ 06.40.05.14.67 ]');
INSERT INTO `logs_changements` VALUES ('109', 'admin', '16-06-2017 - 11:49', 'REUSSI : Modification du téléphone de la Mairie (454654rez) [ nume54 => nume54zer ]');
INSERT INTO `logs_changements` VALUES ('110', 'admin', '16-06-2017 - 11:49', 'REUSSI : Modification de l\'E-mail de la Mairie (454654rez) [ mairie.blaie789e@wanadoo.fr / fortinjacques51@gmail.com => mairie.blaie789fsdf@wanadoo.fr / fortinjacques51@gmail.com ]');
INSERT INTO `logs_changements` VALUES ('111', 'admin', '16-06-2017 - 11:49', 'REUSSI : Modification de l\'adresse de la Mairie (454654rez) [ 13 rue Basse (54651300) => 13 rue Bassezre (54651300) ]');
INSERT INTO `logs_changements` VALUES ('112', 'admin', '16-06-2017 - 11:53', 'REUSSI : Modification du nom du Maire (454654rez) [ Jacques FORTIN => Jacques FORTINrezs ]');
INSERT INTO `logs_changements` VALUES ('113', 'admin', '16-06-2017 - 11:53', 'REUSSI : Suppression de la Mairie \' a \'');
INSERT INTO `logs_changements` VALUES ('114', 'admin', '16-06-2017 - 11:53', 'REUSSI : Suppression de la Mairie \' BIGNICOURT sur Marne \'');
INSERT INTO `logs_changements` VALUES ('115', 'admin', '16-06-2017 - 11:54', 'REUSSI : Suppression de la Mairie \' 454654rez \'');
INSERT INTO `logs_changements` VALUES ('116', 'admin', '16-06-2017 - 11:54', 'REUSSI : Modification du nom de la Mairie (BREBAN1) [ BREBAN => BREBAN1 ]');
INSERT INTO `logs_changements` VALUES ('117', 'admin', '16-06-2017 - 11:54', 'REUSSI : Modification du nom de la Mairie (CHAPELAINE1) [ CHAPELAINE => CHAPELAINE1 ]');
INSERT INTO `logs_changements` VALUES ('118', 'admin', '16-06-2017 - 11:54', 'REUSSI : Modification du nom de la Mairie (CHATELRAOULD1) [ CHATELRAOULD => CHATELRAOULD1 ]');
INSERT INTO `logs_changements` VALUES ('119', 'admin', '16-06-2017 - 11:54', 'REUSSI : Modification du nom du Maire (COOLE) [ José SONGY => José SONGY1 ]');
INSERT INTO `logs_changements` VALUES ('120', 'admin', '16-06-2017 - 11:54', 'REUSSI : Modification de l\'Adresse perso du Maire (Béatrice MIROFLE) [ 27 rue Principale => 27 rue Principale1 ]');
INSERT INTO `logs_changements` VALUES ('121', 'admin', '16-06-2017 - 12:02', 'REUSSI : Ajout de la Mairie \' CHALONS \'');
INSERT INTO `logs_changements` VALUES ('122', 'admin', '16-06-2017 - 15:33', 'REUSSI : Modification du nom de la Mairie (ABLANCOURTS) [ ABLANCOURT => ABLANCOURTS ]');
INSERT INTO `logs_changements` VALUES ('123', 'admin', '16-06-2017 - 15:33', 'REUSSI : Modification du nom du Maire (AULNAY L\\\'AITRE) [ Michel LONCLAS => Michel LONCLASS ]');
INSERT INTO `logs_changements` VALUES ('124', 'admin', '16-06-2017 - 15:34', 'REUSSI : Modification du nom de la Mairie (ABLANCOURT) [ ABLANCOURTS => ABLANCOURT ]');
INSERT INTO `logs_changements` VALUES ('125', 'admin', '16-06-2017 - 15:34', 'REUSSI : Modification du nom du Maire (AULNAY L\\\'AITRE) [ Michel LONCLASS => Michel LONCLAS ]');
INSERT INTO `logs_changements` VALUES ('126', 'admin', '19-06-2017 - 10:31', 'REUSSI : Réinitialisation des compteurs !');

-- ----------------------------
-- Table structure for logs_connexions
-- ----------------------------
DROP TABLE IF EXISTS `logs_connexions`;
CREATE TABLE `logs_connexions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `date` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `mot_de_passe` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `connexion` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of logs_connexions
-- ----------------------------
INSERT INTO `logs_connexions` VALUES ('42', '127.0.0.1', '16-06-2017 - 10:47', 'admin', 'admin', 'REUSSI');
INSERT INTO `logs_connexions` VALUES ('43', '127.0.0.1', '16-06-2017 - 11:10', 'Ryane', 'bouge', 'REUSSI');
INSERT INTO `logs_connexions` VALUES ('44', '127.0.0.1', '16-06-2017 - 11:10', 'Ryane', 'bouge', 'REUSSI');
INSERT INTO `logs_connexions` VALUES ('45', '127.0.0.1', '16-06-2017 - 11:12', 'Ryanb', 'bougueraira', 'ECHOUE');
INSERT INTO `logs_connexions` VALUES ('46', '127.0.0.1', '16-06-2017 - 11:12', 'Ryanb', 'bougueraira', 'REUSSI');
INSERT INTO `logs_connexions` VALUES ('47', '127.0.0.1', '16-06-2017 - 11:13', 'Ryan.B', 'bougueraira', 'ECHOUE');
INSERT INTO `logs_connexions` VALUES ('48', '127.0.0.1', '16-06-2017 - 11:34', 'admin', 'admin', 'ECHOUE');
INSERT INTO `logs_connexions` VALUES ('49', '127.0.0.1', '16-06-2017 - 11:34', 'admin', 'admin', 'REUSSI');
INSERT INTO `logs_connexions` VALUES ('50', '127.0.0.1', '16-06-2017 - 12:02', 'admin', 'admin', 'REUSSI');
INSERT INTO `logs_connexions` VALUES ('51', '127.0.0.1', '16-06-2017 - 14:23', 'admin', 'admin', 'REUSSI');
INSERT INTO `logs_connexions` VALUES ('52', '127.0.0.1', '16-06-2017 - 15:33', 'admin', 'admin', 'REUSSI');
INSERT INTO `logs_connexions` VALUES ('53', '127.0.0.1', '19-06-2017 - 10:25', 'admin', 'admin', 'REUSSI');
INSERT INTO `logs_connexions` VALUES ('54', '127.0.0.1', '19-06-2017 - 10:32', 'admin', 'admin', 'REUSSI');

-- ----------------------------
-- Table structure for mairie
-- ----------------------------
DROP TABLE IF EXISTS `mairie`;
CREATE TABLE `mairie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_mairie` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `nom` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `adresse_perso` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `portable_maire` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `tel_mairie` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `e_mail` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `adresse_mairie` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mairie
-- ----------------------------
INSERT INTO `mairie` VALUES ('1', 'ABLANCOURT', 'Steve BREUZARD', '2 rue de la Bouffannaise', '03.26.41.35.19 / 06.26.39.04.37', '03.26.74.52.12', 'commune.ablancourt@orange.fr / breuzard.steve@orange.fr', '7 rue Perrot d\'Ablancourt (51240)');
INSERT INTO `mairie` VALUES ('2', 'ARZILLIERES Neuville', 'Michel CAPPE', '7 rue des Montieux', '06.79.77.46.11 / 03.26.72.83.79', '03.26.72.84.71', 'mairie.arzillieres@wanadoo.fr / cappe.brigitte@wanadoo.fr', '18 rue de la Mairie (51290)');
INSERT INTO `mairie` VALUES ('3', 'AULNAY L\'AITRE', 'Michel LONCLAS', '27 grand rue ', '03.26.72.98.78 / 06.28.74.82.71', '03.26.72.98.09', 'mairie.aulnaylaitre@wanadoo.fr / michel.lonclas@sfr.fr', '25 grand rue (51240)');
INSERT INTO `mairie` VALUES ('4', 'BLACY', 'Daniel FONTAINE', '8 rue de la Pentecôte', '09.77.09.57.86 / 06.76.59.15.79', '03.26.72.09.22', 'communedeblacy@wanadoo.fr / danjo.fontaine@wanadoo.fr', '4 rue des Tilleuls (51300)');
INSERT INTO `mairie` VALUES ('5', 'BIGNICOURT sur Marne', 'Jean-Pierre FORMET', '13 rue de la Haute Fontaine', '03.26.72.24.55 / 06.75.22.43.72', '03.26.74.07.98', 'mairie-bignicourt@orange.fr / jean-pierre-formet@wanadoo.fr', '2 rue Jean Marie Aubry (51300)');
INSERT INTO `mairie` VALUES ('6', 'Blaise ss Arzillières', 'Jacques FORTIN', '2 avenue de l\'Europe', '03.26.72.81.52 / 06.40.05.14.67', '03.26.72.80.23', 'mairie.blaise@wanadoo.fr / fortinjacques51@gmail.com', '13 rue Basse (51300)');
INSERT INTO `mairie` VALUES ('7', 'BREBAN', 'Bertrand AUDEBERT', '12 grande rue', '03.26.72.07.53 / 07.85.62.38.67', '03.26.73.88.54', 'mairie.breban@wanadoo.fr / bertrand.audebert123@orange.fr', 'Grande rue (51320)');
INSERT INTO `mairie` VALUES ('8', 'CHAPELAINE', 'Philippe DHYEVRE', '8 grande rue', '03.26.72.46.67 / 06.84.31.55.46', '03.26.74.07.37', 'commune.chapelaine@orange.fr / pdhyevre@wanadoo.fr', 'Grande rue (51290)');
INSERT INTO `mairie` VALUES ('9', 'CHATELRAOULD', 'René HANOT', '2 rue Auches', '03.26.72.82.38 / 06.78.96.79.84', '03.26.72.81.94', 'mairie.chatelraould@wanadoo.fr / hanot.rene@wanadoo.fr', '1 rue de l\'Eglise');
INSERT INTO `mairie` VALUES ('10', 'La Chaussee sur Marne', 'Isabelle PESTRE', '29 rue du Colonel Caillot', '06.08.29.74.76', '03.26.72.94.87', 'lachausseesurmarne@wanadoo.fr / francis.pestre@pestre.fr', '17 grande rue de Coulmier (51240)');
INSERT INTO `mairie` VALUES ('11', 'COOLE', 'José SONGY', '5 rue de Songy', '03.26.72.17.85 / 06.08.58.33.41', '03.26.72.33.01', 'mairie.coole@wanadoo.fr / jose.songy@orange.fr', 'rue de la Mairie (51320)');
INSERT INTO `mairie` VALUES ('12', 'CORBEIL', 'Béatrice MIROFLE', '27 rue Principale', '03.26.72.31.04 / 06.42.19.78.01', '03.26.74.29.11', 'mairie.corbeil@orange.fr / miroflebeatrice@voila.fr', '1 rue Cruchière (51320)');
INSERT INTO `mairie` VALUES ('13', 'COURDEMANGES', 'Bigitte HANSE', '38 rue des Auges', '03.26.74.57.60 / 06.82.64.59.25', '03.26.72.29.56', 'communecourdemanges@orange.fr / gisquetteh@orange.fr', 'Grande Rue (51300)');
INSERT INTO `mairie` VALUES ('14', 'COUVROT', 'Jean PANKOW', '4 rue des Vignottes', '03.26.74.08.32 / 06.31.33.68.76', '03.26.74.03.94', 'mairiecouvrot@wanadoo.fr', ' 1 Place de la Mairie (51300)');
INSERT INTO `mairie` VALUES ('15', 'DROUILLY', 'Didier MATHIEU', '56 Grande Rue', '03.26.73.81.30 / 06.86.20.85.06', '03.26.72.78.28', 'mairie.drouilly@wanadoo.fr / didier.mathieu51@orange.fr', 'place de la Mairie (51300)');
INSERT INTO `mairie` VALUES ('16', 'FRIGNICOURT', 'Florian THIERY', '5 résidence Geneviève Devignes', '03.51.54.40.23 / 06.68.75.87.43', '03.26.74.15.08', 'mairie.frignicourt@wanadoo.fr / thieryflorian@orange.fr', '31 rue du Général Leclerc (51300)');
INSERT INTO `mairie` VALUES ('17', 'GLANNES', 'David COLLOR', '42 Grande rue', '03.26.73.76.94 / 06.08.88.69.45', '03.26.72.20.85', 'mairie-glannes@wanadoo.fr / menuiserie.dcollot@orange.fr', 'Grande rue (51300)');
INSERT INTO `mairie` VALUES ('18', 'HUMBAUVILLE', 'Olivier MALOU', '1 rue de la Gare à SOMPUIS', '03.26.73.84.17 / 06.09.31.15.41', '03.26.72.18.91', 'mairie-humbauville@orange.fr / malou.beatrice@wanadoo.fr', '6 rue du Four (51320)');
INSERT INTO `mairie` VALUES ('19', 'HUIRON', 'Jacky DESBROSSE', '35 rue Saint Martin', '03.26.72.05.95 / 06.80.99.47.29', '03.26.41.04.19', 'mairie.huiron@wanadoo.fr / jackie.desbrosse@free.fr', 'Rue Saint Martin (51300)');
INSERT INTO `mairie` VALUES ('20', 'LIGNON', 'Guy JACQUEMIN', 'route de Chapelaine', '06.80.43.45.47 / 03.26.72.47.55', '03.26.74.39.32', 'communedelignon@orange.fr / jacquemin.guy51@sfr.fr', 'Grande rue (51290)');
INSERT INTO `mairie` VALUES ('21', 'LOISY sur Marne', 'Jean-Pol BESNARD', '4 chemin des Vignes', '06.42.48.09.27', '03.26.72.03.17', 'mairie.loisysurmarne@orange.fr / jeanpol.besnard@gmail.com', '98 rue Choiset (51300)');
INSERT INTO `mairie` VALUES ('22', 'MAISONS en Champagne', 'Christian MOULIN', '24 rue de Coole', '03.26.72.73.80 / 06.73.89.10.90', '03.26.72.77.47', 'mairie.maisonenchampagne@wanadoo.fr / lesmoulinsdemaison@wanadoo.fr', 'rue Flancourt (51300)');
INSERT INTO `mairie` VALUES ('23', 'MARGERIE HANCOURT', 'Michelle GEOFFROY', '16 Route d\'Hancourt', '03.26.72.48.47 / 06.28.29.21.44', '03.26.72.77.03', 'denis.geoffroy@wanadoo.fr / commune-margerie-hancourt@wanadoo.fr', '16 rue Julien Rousselet (51290)');
INSERT INTO `mairie` VALUES ('24', 'LE MEIX TIERCELIN', 'René MAUTRAIT', 'rue Bas', '06.78.43.47.44 / 03.26.72.47.33', '03.26.72.73.42', 'mairie.lemeixtiercelin@orange.fr / rene.mautrait526@orange.fr', '9 rue Chanterelle (51320)');
INSERT INTO `mairie` VALUES ('25', 'MAROLLES', 'Didier NOBLET', '24 rue Aristide Briand', '06.80.18.71.93', '03.26.74.07.21', 'mairie.51marolles@wanado.fr / nobletdidier@gmail.com', 'rue Charmilles (51300)');
INSERT INTO `mairie` VALUES ('26', 'PRINGY', 'Michel ROUDIER', '4 rue de Jeannette', '03.26.72.62.68 / 06.47.03.65.35', '03.26.72.77.77', 'mairiedepringy51@wanadoo.fr / michel.roudier7@orange.fr', '22 grande rue (51300)');
INSERT INTO `mairie` VALUES ('27', 'Les RIVIERES Henruel', 'Patrick CHAMPION', '18 rue Jumeret', '03.26.72.84.40', '03.26.72.83.63', 'commune.riviereshenruel@orange.fr / patrick51.champion@laposte.net', '6 rue Jumeret (51300)');
INSERT INTO `mairie` VALUES ('28', 'SAINT CHERON', 'Marylène SIMONNET', '1 ruelle Glaie', '03.26.72.80.28 / 06.37.27.45.18', '03.26.72.83.74', 'mairie.stcheron@orange.fr / simonnet.mary@orange.fr', '20 grande rue (51290)');
INSERT INTO `mairie` VALUES ('29', 'SAINT OUEN DOMPROT', 'Philippe COQUIN', '8 rue Saint Etienne', '03.26.72.30.97 / 06.23.82.87.22', '03.26.72.31.34', 'mairiestouen@orange.fr / bechamp@hotmail.fr', '28 Grande Rue (51320)');
INSERT INTO `mairie` VALUES ('30', 'SAINT UTIN', 'Philippe ROYER', '3 rue de Saint Léger', '03.26.72.77.32 / 06.27.01.35.08', '03.26.72.49.36', 'commune.st.utin@orange.fr / royer51@laposte.net', '2 rue Haute (51290)');
INSERT INTO `mairie` VALUES ('31', 'SOMPUIS', 'Eric CHAVEROU', '14 rue d\'Humbauville', '03.26.74.74.01 / 06.14.52.71.93', '03.26.72.41.49', 'mairie.sompuis@orange.fr / eric.chaverou@orange.fr', '2 rue de l\'Eglise (51320)');
INSERT INTO `mairie` VALUES ('32', 'SOMSOIS', 'Joël LOISELET', '1 ruelle du château', '03.26.72.46.55 / 06.89.72.56.01', '03.26.74.37.68', 'mairie-somsois@orange.fr', '8 Grande Rue (51290)');
INSERT INTO `mairie` VALUES ('33', 'SONGY', 'Francis PASSINHAS', '32 Grande Rue', '03.26.74.02.19 / 06.80.55.38.74', '03.26.72.64.15', 'mairie.songy@wanadoo.fr / sfpassinhas@orange.fr', 'grande rue (51240)');
INSERT INTO `mairie` VALUES ('34', 'SOULANGES', 'Raymond LATREUILLE', '1 Impasse Loisson', '03.26.41.07.55 / 06.76.48.10.09', '03.26.72.78.73', 'mairie.soulanges@anadoo.fr / nicole.latreuille@wanadoo.fr', '22 bis rue Louis Guibert (51300)');

-- ----------------------------
-- Table structure for service
-- ----------------------------
DROP TABLE IF EXISTS `service`;
CREATE TABLE `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categorie_id` int(11) DEFAULT NULL,
  `nom` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of service
-- ----------------------------
INSERT INTO `service` VALUES ('1', '1', 'Médiathèque Albert Camus');
INSERT INTO `service` VALUES ('2', '1', 'CCAS');
INSERT INTO `service` VALUES ('3', '1', 'Maison de la Petite Enfance');
INSERT INTO `service` VALUES ('4', '1', 'Réussite Educative');
INSERT INTO `service` VALUES ('5', '1', 'S.T. Municipaux');
INSERT INTO `service` VALUES ('6', '1', 'Médiathèque Francois Mitterrand');
INSERT INTO `service` VALUES ('7', '1', 'Ecole de Musique');
INSERT INTO `service` VALUES ('8', '1', 'Police Municipal');
INSERT INTO `service` VALUES ('9', '1', 'Lucien Herr');
INSERT INTO `service` VALUES ('10', '1', 'Orange Bleu');
INSERT INTO `service` VALUES ('11', '1', 'Informatique');
INSERT INTO `service` VALUES ('12', '2', 'Dir. Développement Economique');
INSERT INTO `service` VALUES ('13', '2', 'S.T. Communautaires');
INSERT INTO `service` VALUES ('14', '2', 'Office du Tourisme');
INSERT INTO `service` VALUES ('15', '2', 'Déchetterie');
INSERT INTO `service` VALUES ('16', '3', 'Centre Médico Scolaire (CMS)');
INSERT INTO `service` VALUES ('17', '3', 'Jules');
INSERT INTO `service` VALUES ('18', '3', 'Verne');
INSERT INTO `service` VALUES ('19', '3', 'Paul Fort');
INSERT INTO `service` VALUES ('20', '3', 'Ferdinand Buisson');
INSERT INTO `service` VALUES ('21', '3', 'Jules Ferry');
INSERT INTO `service` VALUES ('22', '3', 'Pierre et Marie Curie');
INSERT INTO `service` VALUES ('23', '3', 'Louis Pasteur');
INSERT INTO `service` VALUES ('24', '3', 'Ecoles Communautés de Communes');
INSERT INTO `service` VALUES ('25', '4', 'Affaires Scolaires');
INSERT INTO `service` VALUES ('26', '4', 'Affaires Culturelles');
INSERT INTO `service` VALUES ('27', '4', 'C.U.C.S');
INSERT INTO `service` VALUES ('28', '4', 'PRU');
INSERT INTO `service` VALUES ('29', '4', 'Urbanisme');
INSERT INTO `service` VALUES ('30', '5', 'Affaires générales');
INSERT INTO `service` VALUES ('31', '5', 'Communication');
INSERT INTO `service` VALUES ('32', '5', 'Ressources Humaines');
INSERT INTO `service` VALUES ('33', '5', 'Finances comptabilité');
INSERT INTO `service` VALUES ('34', '5', 'Hygiène et sécurité');
INSERT INTO `service` VALUES ('35', '5', 'Cabinet du Maire');
INSERT INTO `service` VALUES ('36', '5', 'Secrétariat générale');
INSERT INTO `service` VALUES ('37', '5', 'Elus');
INSERT INTO `service` VALUES ('38', '5', 'Cabinet CC');
