<?php

session_start();

$ip = "127.0.0.1";
$user_mysql = "pakrologie";
$mdp_mysql = "noproblemo5";
$db = "annuaire";

$mysqli = new mysqli($ip, $user_mysql, $mdp_mysql, $db);

$mysqli->query("SET CHARSET utf8");

redirect();

	function redirect()
	{
		if (!isAdmin())
		{
			header("Location: Home.php?change=failed");
			return;
		}

		if (isset($_GET['nom_categorie']) && isset($_GET['nom_service']))
		{
			$log = addInCategorie();

			if (strlen($log) == 0)
			{
				header("Location: Home.php?change=failed");
			}else
			{
				header("Location: Home.php?change=success&log=$log");
			}

			return;
		}
		else if (isset($_GET['ajouter_nom']) && isset($_GET['ajouter_numero']) && isset($_GET['service']))
		{
			$log = addData();

			if (strlen($log) == 0)
			{
				header("Location: Home.php?change=failed");
			}else
			{
				header("Location: Home.php?change=success&log=$log");
			}

			return;
		}
		else if (isset($_GET['delete_tr_id']) && isset($_GET['delete_tr_numero']))
		{
			$log = deleteData();

			if (strlen($log) == 0)
			{
				header("Location: Home.php?change=failed");
			}else
			{
				header("Location: Home.php?change=success&log=$log");
			}
			return;
		}else if (isset($_GET['DeleteLogsCompteurs']))
		{
			$log = DeleteLogsCompteurs();
			if (strlen($log) == 0)
			{
				header("Location: Home.php?change=failed");
			}else
			{
				header("Location: Home.php?change=success&log=$log");
			}
			return;
		}else if (isset($_GET['DeleteLogsChangements']))
		{
			$log = DeleteLogsChangements();
			if (strlen($log) == 0)
			{
				header("Location: Home.php?change=failed");
			}else
			{
				header("Location: Home.php?change=success&log=$log");
			}
			return;
		}else if (isset($_GET['DeleteLogsConnexions']))
		{
			$log = DeleteLogsConnexions();
			if (strlen($log) == 0)
			{
				header("Location: Home.php?change=failed");
			}else
			{
				header("Location: Home.php?change=success&log=$log");
			}
			return;
		}else if (isset($_GET['delete_service']))
		{
			$log = Deleteservice();
			if (strlen($log) == 0)
			{
				header("Location: Home.php?change=failed");
			}else
			{
				header("Location: Home.php?change=success&log=$log");
			}
			return;
		}else if (isset($_GET['nom_mairie']) && isset($_GET['nom_maire']) && isset($_GET['adresse_perso']) && isset($_GET['portable_maire']) 
			   && isset($_GET['tel_mairie']) && isset($_GET['e-mail']) && isset($_GET['adresse_mairie']))
		{
			$log = addMairie();

			if (strlen($log) == 0)
			{
				header("Location: Home.php?change=failed");
			}else
			{
				header("Location: Home.php?change=success&log=$log");
			}
			return;
		}else if (isset($_GET['delete_mairie']) && isset($_GET['delete_mairie_id']))
		{
			$log = deleteMairie();

			if (strlen($log) == 0)
			{
				header("Location: Home.php?change=failed");
			}else
			{
				header("Location: Home.php?change=success&log=$log");
			}
			return;
		}
		else if (isset($_GET['0x0'])) // TODO : Revoir les paramètres ? 
		{
			$log = saveMairie();

			if (strlen($log) == 0)
			{
				header("Location: Home.php?change=failed");
			}else
			{
				header("Location: Home.php?change=success&log=$log");
			}
			return;
		}
		else
		{
			// TODO : Créer la condition
			$log = saveData();

			if (strlen($log) == 0)
			{
				header("Location: Home.php?change=failed");
			}else
			{
				header("Location: Home.php?change=success&log=$log");
			}
		}
	}

	function isAdmin()
	 {
		 return (isset($_SESSION['login']) && isset($_SESSION['password']));
	 }

	 function saveMairie()
	 {
		try
		{
			global $mysqli;

			$log = "";

			$nom_mairie = "";
			$nom_maire = "";
			$adresse_perso = "";
			$portable_maire = "";
			$tel_mairie = "";
			$e_mail = "";
			$adresse_mairie = "";

			$count = 0;

			foreach($_GET as $key => $value)
			{
				$count++;

				$key = getCleanStr($key, $mysqli);

				$value = getCleanStr($value, $mysqli);

				switch($count)
				{
					case 1:
						$id = getIdByKey($key);
						$nom_mairie = $value;
						break;
					case 2:
						$nom_maire = $value;
						break;
					case 3:
						$adresse_perso = $value;
						break;
					case 4:
						$portable_maire = $value;
						break;
					case 5:
						$tel_mairie = $value;
						break;
					case 6:
						$e_mail = $value;
						break;
					case 7:
						$adresse_mairie = $value;

						if (!nom_mairieExist($nom_mairie))
						{
							$query = "UPDATE mairie SET nom_mairie=\"$nom_mairie\" WHERE id='$id'";
							
							$old_nom_mairie = GetInfoById($id, "nom_mairie");

							if ($mysqli->query($query))
							{
								$log .= "REUSSI : Modification du nom de la Mairie ($nom_mairie) [ $old_nom_mairie => $nom_mairie ]";
								$log .= "<br/>";
							}
						}
						if (!nom_maireExist($nom_maire))
						{
							$query = "UPDATE mairie SET nom=\"$nom_maire\" WHERE id='$id'";
							
							$old_nom_maire = GetInfoById($id, "nom");

							if ($mysqli->query($query))
							{
								$log .= "REUSSI : Modification du nom du Maire ($nom_mairie) [ $old_nom_maire => $nom_maire ]";
								$log .= "<br/>";
							}
						}
						if (!adresse_persoExist($adresse_perso))
						{
							$query = "UPDATE mairie SET adresse_perso=\"$adresse_perso\" WHERE id='$id'";
							
							$old_adresse_perso = GetInfoById($id, "adresse_perso");

							if ($mysqli->query($query))
							{
								$log .= "REUSSI : Modification de l'Adresse perso du Maire ($nom_maire) [ $old_adresse_perso => $adresse_perso ]";
								$log .= "<br/>";
							}
						}
						if (!portable_maireExist($portable_maire))
						{
							$query = "UPDATE mairie SET portable_maire=\"$portable_maire\" WHERE id='$id'";
							
							$old_portable_maire = GetInfoById($id, "portable_maire");

							if ($mysqli->query($query))
							{
								$log .= "REUSSI : Modification du portable du Maire ($nom_maire) [ $old_portable_maire => $portable_maire ]";
								$log .= "<br/>";
							}
						}
						if (!tel_mairieExist($tel_mairie))
						{
							$query = "UPDATE mairie SET tel_mairie=\"$tel_mairie\" WHERE id='$id'";
							
							$old_tel_mairie = GetInfoById($id, "tel_mairie");

							if ($mysqli->query($query))
							{
								$log .= "REUSSI : Modification du téléphone de la Mairie ($nom_mairie) [ $old_tel_mairie => $tel_mairie ]";
								$log .= "<br/>";
							}
						}
						if (!e_mailExist($e_mail))
						{
							$query = "UPDATE mairie SET e_mail=\"$e_mail\" WHERE id='$id'";
							
							$old_e_mail = GetInfoById($id, "e_mail");

							if ($mysqli->query($query))
							{
								$log .= "REUSSI : Modification de l'E-mail de la Mairie ($nom_mairie) [ $old_e_mail => $e_mail ]";
								$log .= "<br/>";
							}
						}
						if (!adresse_mairieExist($adresse_mairie))
						{
							$query = "UPDATE mairie SET adresse_mairie=\"$adresse_mairie\" WHERE id='$id'";
							
							$old_adresse_mairie = GetInfoById($id, "adresse_mairie");

							if ($mysqli->query($query))
							{
								$log .= "REUSSI : Modification de l'adresse de la Mairie ($nom_mairie) [ $old_adresse_mairie => $adresse_mairie ]";
								$log .= "<br/>";
							}
						}

						$count = 0;
						break;
				}

			}
			
			return $log;
		}catch(Exception $e)
		{
			return "";
		}
	 }

	 function deleteMairie()
	 {
		 try
		 {
			global $mysqli;

			$log = "";

			$nom_mairie = getCleanStr($_GET['delete_mairie'], $mysqli);
			$id = getCleanStr($_GET['delete_mairie_id'], $mysqli);

			if (empty($nom_mairie) || empty($id))
			{
				return "";
			}

			$query = "DELETE FROM mairie WHERE id=$id And nom_mairie='$nom_mairie'";

			if ($mysqli->query($query))
			{
				$log .= "REUSSI : Suppression de la Mairie ' $nom_mairie '";
				$log .= "<br/>";
				return $log;
			}else
			{
				return "";
			}
		 }catch(Exception $e)
		 {
			 return "";
		 }
	 }

	 function addMairie()
	 {
		 try
		{
			global $mysqli;
			$log = "";

			$nom_mairie = getCleanStr($_GET['nom_mairie'], $mysqli);
			$nom_maire = getCleanStr($_GET['nom_maire'], $mysqli);
			$adresse_perso = getCleanStr($_GET['adresse_perso'], $mysqli);
			$portable_maire = getCleanStr($_GET['portable_maire'], $mysqli);
			$tel_mairie = getCleanStr($_GET['tel_mairie'], $mysqli);
			$email = getCleanStr($_GET['e-mail'], $mysqli);
			$adresse_mairie = getCleanStr($_GET['adresse_mairie'], $mysqli);

			if (empty($nom_mairie) || empty($nom_maire) || empty($adresse_perso) || empty($portable_maire) || empty($tel_mairie) || empty($email) || empty($adresse_mairie))
			{
				return "";
			}

			if (infoMairieExist($nom_mairie, $nom_maire, $adresse_perso, $portable_maire, $tel_mairie, $email, $adresse_mairie))
			{
				return "";
			}

			$query = "INSERT INTO mairie (nom_mairie, nom, adresse_perso, portable_maire, tel_mairie, e_mail, adresse_mairie) VALUES ('$nom_mairie', '$nom_maire', '$adresse_perso', '$portable_maire', '$tel_mairie', '$email', '$adresse_mairie')";
			
			if ($mysqli->query($query))
			{
				$log .= "REUSSI : Ajout de la Mairie ' $nom_mairie '";
				$log .= "<br/>";
				return $log;
			}else
			{
				return "";
			}
		}catch(Exception $e)
		{
			return "";
		}
	 }

	 function DeleteDataFromService($service_nom)
	 {
		global $mysqli;

		$query = "SELECT * FROM service WHERE nom='$service_nom'";

		$result = $mysqli->query($query);

		if ($result->num_rows > 0)
		{
			$row = $result->fetch_assoc();
			$service_id = getCleanStr($row['id'], $mysqli);

			$query = "DELETE FROM annuaire_data WHERE service_id=$service_id";
			if ($mysqli->query($query))
			{
				return true;
			}
		}
		return false;
	 }

	 function Deleteservice()
	 {
		 try
		 {
			global $mysqli;

			$log = "";

			$nom = getCleanStr($_GET['delete_service'], $mysqli);

			if (empty($nom))
			{
				return "";
			}

			$query = "DELETE FROM service WHERE nom='$nom'";
			
			if (DeleteDataFromService($nom))
			{
				$log .= "REUSSI : Suppression des données liées aux services ' $nom '";
				$log .= "<br/>";
			}

			if ($mysqli->query($query))
			{
				$log .= "REUSSI : Suppression du service ' $nom '";
				$log .= "<br/>";
	
				return $log;
			}else
			{
				return "";
			}
		 }catch(Exception $e)
		 {
			return "";
		 }	
	 }

	 function DeleteLogsCompteurs()
	 {
		 try
		 {
			global $mysqli;

			$log = "";

			$query = "DELETE FROM compteurs";

			if ($mysqli->query($query))
			{
				$log .= "REUSSI : Réinitialisation des compteurs !";
				$log .= "<br/>";
				
			}else
			{
				return "";
			}
			return $log;
		 }catch(Exception $e)
		 {
			return "";
		 }
	 }

	 function DeleteLogsChangements()
	 {
		 try
		 {
			global $mysqli;

			$log = "";

			$query = "DELETE FROM logs_changements";

			if ($mysqli->query($query))
			{
				$log .= "REUSSI : Réinitialisation des logs de changements !";
				$log .= "<br/>";
				
			}else
			{
				return "";
			}
			return $log;
		 }catch(Exception $e)
		 {
			return "";
		 }
	 }

	 function DeleteLogsConnexions()
	 {
		 try
		 {
			global $mysqli;

			$log = "";

			$query = "DELETE FROM logs_connexions";

			if ($mysqli->query($query))
			{
				$log .= "REUSSI : Réinitialisation des logs de connexions !";
				$log .= "<br/>";
				
			}else
			{
				return "";
			}
			return $log;
		 }catch(Exception $e)
		 {
			return "";
		 }
	 }

	 function addInCategorie()
	 {
		 try
		 {
			global $mysqli;

			$log = "";

			$nom_categorie = getCleanStr($_GET['nom_categorie'], $mysqli);
			$nom_service = getCleanStr($_GET['nom_service'], $mysqli);
			$categorie_id = getid_bycategorie($nom_categorie);
			
			if (serviceExist($nom_service) || $categorie_id == "-" || empty($nom_categorie) || empty($nom_service))
			{
				return "";
			}

			$query = "INSERT INTO service (categorie_id, nom) VALUES ('$categorie_id', '$nom_service')";
			
			if (!$mysqli->query($query))
			{
				return "";
			}else
			{
				$log .= "REUSSI : Ajout du service ' $nom_service ' dans la catégorie ' $nom_categorie ' !";
				$log .= "<br/>";
			}

			return $log;
		 }catch(Exception $e)
		 {
			 return "";
		 }
	 }

	 function addData()
	 {
		 try
		 {
			global $mysqli;

			$log = "";

			$service = getCleanStr($_GET['service'], $mysqli);
			$nom = getCleanStr($_GET['ajouter_nom'], $mysqli);
			$numero = getCleanStr($_GET['ajouter_numero'], $mysqli);
			
			$service_id = getserviceid_bynom($service);

			if (nameExist($nom) || numeroExist($numero) || $service_id == "-" || empty($service) || empty($nom) || empty($numero))
			{
				return "";
			}

			$query = "INSERT INTO annuaire_data (service_id, nom, numero) VALUES ('$service_id', '$nom', '$numero')";

			if (!$mysqli->query($query))
			{
				return "";
			}else
			{
				$log .= "REUSSI : Ajout de ' $nom ' avec le numéro [ $numero ] !";
				$log .= "<br/>";
			}

			return $log;
		 }catch(Exception $ex)
		 {
			 return "";
		 }
	 }

	 function deleteData()
	 {
		try
		{
			global $mysqli;
			
			$log = "";

			$id = getCleanStr($_GET['delete_tr_id'], $mysqli);

			$nom = getNomById($id);

			if ($nom == "")
			{
				return "";
			}

			$query = "DELETE FROM annuaire_data WHERE id='$id'";

			if (!$mysqli->query($query))
			{
				return "";
			}else
			{
				$log .= "REUSSI : Suppression de $nom !";
				$log .= "<br/>";
			}

			return $log;
		}catch(Exception $e)
		{
			return "";
		}
	 }

   	function saveData()
	{
		try
		{
			if ((count($_GET)) % 2 != 0)
			{
				return "";
			}

			global $mysqli;

			$log = "";

			$count = 0;
			$id = "";
			$new_nom = "";
			$new_numero = "";

			foreach($_GET as $key => $value)
			{
				$count++;
				
				$key = getCleanStr($key, $mysqli);

				$value = getCleanStr($value, $mysqli);

				if ($count == 1)
				{
					$id = $key;
					$new_nom = $value;
				}
				else if ($count == 2)
				{
					$id = getIdByKey($id);

					$new_numero = $value;

					if (!nameExist($new_nom))
					{
						$old_nom = getNomById($id);

						if (empty($old_nom) || empty($new_nom))
						{
							return "";
						}

						$query = "UPDATE annuaire_data SET nom=\"" . $new_nom . "\" WHERE id='" . $id . "'";

						if (!$mysqli->query($query))
						{
							$log .= "ECHOUE : Modification des informations de [ $old_nom => $new_nom ] !";
							$log .= "<br/>";
						}else
						{
							$log .= "REUSSI : Modification des informations de [ $old_nom => $new_nom ] !";
							$log .= "<br/>";
						}
					}

					if (!numeroExist($new_numero))
					{
						$old_numero = getNumeroById($id);

						if (empty($old_numero) || empty($new_numero))
						{
							return "";
						}

						$query = "UPDATE annuaire_data SET numero=\"$new_numero\" WHERE id='" . $id . "'";

						if (!$mysqli->query($query))
						{
							$log .= "ECHOUE : Modification du numéro de : $new_nom [ $old_numero => $new_numero ] !";
							$log .= "<br/>";
						}else
						{
							$log .= "REUSSI : Modification du numéro de : $new_nom [ $old_numero => $new_numero ] !";
							$log .= "<br/>";
						}
					}
					
					$count = 0;
					$nom_original = "";
					$new_numero = "";
					$new_nom = "";
				}
			}
			return $log;
		}catch(Exception $e)
		{
			return "";
		}
	}

	function getIdByKey($key)
	{
		return explode('_', $key)[1];
	}


	function getCleanStr($str, $mysqli)
	{
		$str =  mysqli_real_escape_string($mysqli, addcslashes($str, "%_"));
		return $str;
	}

	function nameExist($nom)
	{
		global $mysqli;

		$query = "SELECT * FROM annuaire_data WHERE nom='$nom'";

		$result = $mysqli->query($query);

		if ($result->num_rows > 0)
		{
			return true;
		}

		return false;
	}

	function getserviceid_bynom($nom)
	{
		global $mysqli;

		$result = $mysqli->query("SELECT * FROM service WHERE nom='$nom'");

		if ($result->num_rows > 0)
		{
			$row = $result->fetch_assoc();
			$service_id = $row['id'];

			return $service_id;
		}
		return "-";
	}

	function getNomById($id)
	{
		global $mysqli;


		$query = "SELECT * FROM annuaire_data WHERE id='$id'";

		$result = $mysqli->query($query);

		if ($result->num_rows > 0)
		{
			$row = $result->fetch_assoc();

			return $row['nom'];
		}

		return "";
	}

	function getNumeroById($id)
	{
		global $mysqli;

		$query = "SELECT * FROM annuaire_data WHERE id='$id'";

		$result = $mysqli->query($query);

		if ($result->num_rows > 0)
		{
			$row = $result->fetch_assoc();

			return $row['numero'];
		}

		return "";
	}

	function numeroExist($numero)
	{
		global $mysqli;

		$query = "SELECT * FROM annuaire_data WHERE numero='$numero'";

		$result = $mysqli->query($query);

		if ($result->num_rows > 0)
		{
			return true;
		}

		return false;
	}

	function serviceExist($nom_service)
	{
		global $mysqli;

		$query = "SELECT * FROM service WHERE nom='$nom_service'";

		$result = $mysqli->query($query);

		if ($result->num_rows > 0)
		{
			return true;
		}

		return false;
	}

	function getid_bycategorie($nom)
	{
		global $mysqli;

		$result = $mysqli->query("SELECT * FROM categorie WHERE nom='$nom'");

		if ($result->num_rows > 0)
		{
			$row = $result->fetch_assoc();
			$categorie_id = $row['id'];

			return $categorie_id;
		}
		return "-";
	}

															/* FONCTIONS DE VERIFICATIONS POUR MAIRIE */

	function infoMairieExist($nom_mairie, $nom_maire, $adresse_perso, $portable_maire, $tel_mairie, $email, $adresse_mairie)
	{
		global $mysqli;

		$result = $mysqli->query("SELECT * FROM mairie WHERE nom_mairie='$nom_mairie' OR nom='$nom_maire' OR adresse_perso='$adresse_perso' OR portable_maire='$portable_maire' OR tel_mairie='$tel_mairie' OR e_mail='$email' OR adresse_mairie='$adresse_mairie'");

		if ($result->num_rows > 0)
		{
			return true;
		}

		return false;
	}

	function nom_mairieExist($nom_mairie)
	{
		global $mysqli;

		$result = $mysqli->query("SELECT * FROM mairie WHERE nom_mairie='$nom_mairie'");
	
		if ($result->num_rows > 0)
		{
			return true;
		}

		return false;
	}

	function nom_maireExist($nom_maire)
	{
		global $mysqli;

		$result = $mysqli->query("SELECT * FROM mairie WHERE nom='$nom_maire'");
	
		if ($result->num_rows > 0)
		{
			return true;
		}

		return false;
	}

	function adresse_persoExist($adresse_perso)
	{
		global $mysqli;

		$result = $mysqli->query("SELECT * FROM mairie WHERE adresse_perso='$adresse_perso'");
	
		if ($result->num_rows > 0)
		{
			return true;
		}

		return false;
	}

	function portable_maireExist($portable_maire)
	{
		global $mysqli;

		$result = $mysqli->query("SELECT * FROM mairie WHERE portable_maire='$portable_maire'");
	
		if ($result->num_rows > 0)
		{
			return true;
		}

		return false;
	}

	function tel_mairieExist($tel_mairie)
	{
		global $mysqli;

		$result = $mysqli->query("SELECT * FROM mairie WHERE tel_mairie='$tel_mairie'");
	
		if ($result->num_rows > 0)
		{
			return true;
		}

		return false;
	}

	function e_mailExist($e_mail)
	{
		global $mysqli;

		$result = $mysqli->query("SELECT * FROM mairie WHERE e_mail='$e_mail'");
	
		if ($result->num_rows > 0)
		{
			return true;
		}

		return false;
	}

	function adresse_mairieExist($adresse_mairie)
	{
		global $mysqli;

		$result = $mysqli->query("SELECT * FROM mairie WHERE adresse_mairie='$adresse_mairie'");
	
		if ($result->num_rows > 0)
		{
			return true;
		}

		return false;
	}


	//------------------------------------------------------------------------

	function GetInfoById($id, $info)
	{
		global $mysqli;

		$result = $mysqli->query("SELECT * FROM mairie WHERE id='$id'");

		if ($result->num_rows > 0)
		{
			$row = $result->fetch_assoc();
			return $row[$info];
		}

		return "";
	}

?>