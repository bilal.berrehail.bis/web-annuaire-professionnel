function deleteData(id, nom, numero)
{
    if (confirm("Êtes-vous sûre de vouloir supprimer la ligne [" + nom + " - " + numero + "]"))
    {
        var currentLocation =  document.location.href;
        window.location = "ChangesData.php?delete_tr_id=" + id + "&delete_tr_numero=" + numero;
    }
}

function deleteDataMairie(id, mairie)
{
    if (confirm("Êtes-vous sûre de vouloir supprimer la Mairie [" + mairie + "]"))
    {
        var currentLocation =  document.location.href;
        window.location = "ChangesData.php?delete_mairie=" + mairie + "&delete_mairie_id=" + id;
    }
}

/*function ajouterLigne() // NON UTILISER POUR LE MOMENT
{
    var tr = document.createElement('tr');

    var table = document.getElementById('tbl_data');
    table.appendChild(tr);

    var td = document.createElement('td');
    //td.innerHtml = "<input name=\"\" id=\"inputAdmin\" type=\"text\" value=\"$nom\">";

    tr.appendChild(td);
}*/