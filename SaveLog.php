<?php
session_start();

$ip = "127.0.0.1";
$user_mysql = "pakrologie";
$mdp_mysql = "noproblemo5";
$db = "annuaire";

$mysqli = new mysqli($ip, $user_mysql, $mdp_mysql, $db);
$mysqli->query("SET CHARSET utf8");

if (!isAdmin())
{
    return;
}

if (isset($_GET['changements']))
{
    SaveLogsChangements();
}else if (isset($_GET['connexions']))
{
    SaveLogsConnexion();
}

	function isAdmin()
	 {
		 return (isset($_SESSION['login']) && isset($_SESSION['password']));
	 }

     function SaveLogsChangements()
     {
         global $mysqli;

         $log = "";

         $query = "SELECT * FROM logs_changements";

         $result = $mysqli->query($query);
         
         while ($row = $result->fetch_assoc())
         {
             $username = $row['username'];
             $date = $row['date'];
             $changement = $row['changement'];

             $log .= "Username : $username | Date : $date | Changement : $changement";
             $log .= "\n";
         }
         
         download($log, "Logs changements", "lch");
     }

     function SaveLogsConnexion()
     {
         global $mysqli;

         $log = "";

         $query = "SELECT * FROM logs_connexions";

         $result = $mysqli->query($query);
         
         while ($row = $result->fetch_assoc())
         {
             $ip = $row['ip'];
             $date = $row['date'];
             $username = $row['username'];
             $mot_de_passe = $row['mot_de_passe'];
             $connexion = $row['connexion'];

             $log .= "Ip : $ip | Date : $date | Username : $username | Mot de passe : $mot_de_passe ($connexion)";
             $log .= "\n";
         }

         download($log, "Logs connexions", "lco");
     }

     function download($log, $nom_file, $tag)
     {
        date_default_timezone_set('Europe/Paris');

		$date = (date("d-m-Y") . " - " . date("H:i"));
        
        $file = fopen("$tag.txt", "w");
        fwrite($file, $log);
        fclose($file);

        $fichier = "$tag.txt";

        $nom = $nom_file . " : " . $date . ".txt";

        header('Content-Type: application/octet-stream');
        header('Content-Length: ' . strlen($log));
        header('Content-disposition: attachment; filename='. $nom);
        header('Pragma: no-cache');
        header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');
        readfile($fichier);

        unlink("$tag.txt");
      
        exit();
     }
?>