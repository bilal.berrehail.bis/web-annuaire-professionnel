Projet développé en PHP avec base de données MySQL.

Le but était de créer un annuaire numérique professionnel.
Fonctionnalités :
- Gestion des utilisateurs (Ajout, suppression et modification des droits)
- Système de création de catégories et d'ajout de numéro associé à des noms dans ses catégories
- Modifications des catégories et des numéros associés
- Log de chaque changement de données sur le site
- Log de chaque connexion
- Page d'accueil modifiable avec des composants personnalisés
- Système de recherche rapide
