<?php

session_start();

$ip = "127.0.0.1";
$user_mysql = "pakrologie";
$mdp_mysql = "noproblemo5";
$db = "annuaire";

$mysqli = new mysqli($ip, $user_mysql, $mdp_mysql, $db);
$mysqli->query("SET CHARSET utf8");


$adresse = "http://".$_SERVER['SERVER_NAME'];
$server_name = $_SERVER['SERVER_NAME'];
$request_uri = $_SERVER['REQUEST_URI'];

$adresse = "http://" . $server_name . $request_uri;

$home_key = "/Annuaire/Home.php";


if ($request_uri == $home_key)
{
	addViewOnUrl("Page d'accueil");
}
	
?>

<!DOCTYPE html>
<html lang="en">

	<head>


		<title>Annuaire</title>
		<meta charset="utf-8">
		
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="icon" type="image/png" size="32x32" href="img/favicon-32x32.png">
		<link rel="manifest" href="img/manifest.json">
		<link rel="stylesheet" type="text/css" href="css/home.css">
		<link rel="stylesheet" type="text/css" href="http://cdn.phpoll.com/css/animate.css">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/script.js"></script>




	</head>

	<body class="body">

			<nav class="navbar navbar-inverse navbar-fixed-top">
			
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand">Annuaire Téléphonique</a>
				</div>
				<ul class="nav navbar-nav">
					<li class="active"><a href="Home.php"><span class="glyphicon glyphicon-home"></span></a></li>
					
<?php

	afficherCategories();

?>

				<li><a href="?categ=MAIRIE">Mairie CC</a></li>
					
				</ul>
				<form class="navbar-form navbar-left" method="get" action="">
					<div class="input-group">
						<input type="text" name="search" class="form-control" placeholder="Rechercher">
							<div class="input-group-btn">
								<button class="btn btn-default" type="submit">
									<i class="glyphicon glyphicon-search"></i>
								</button>
							</div>
					</div>
				</form>

				<ul class="nav navbar-nav navbar-right">
<?php

if (isset($_POST['username']) && isset($_POST['password'])) // Tentative de connexion
{
	tentativeConnexion($_POST['username'], $_POST['password']);
}

if (isAdmin()) // Connecté
{
?>

					<!-- Single button -->
					<div class="btn-group">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-top:9px; margin-right:8px;"> <span class="glyphicon glyphicon-cog"></span>
						</button>
						<ul class="dropdown-menu">
							<li><h4>&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;Bienvenue, <?php if (isAdmin()){echo $_SESSION['login'];} ?></h4></li>
							<li role="separator" class="divider"></li>
							<li><a href="?logs_connexions="><span class="glyphicon glyphicon-globe"></span> Logs connexions</a></li>
							<li><a href="?logs_changements="><span class="glyphicon glyphicon-refresh"></span> Logs changements</a></li>
							<li><a href="?compteurs="><span class="glyphicon glyphicon-eye-open"></span> Compteurs</a></li>
							<li><a href="?gererService="><span class="glyphicon glyphicon-list-alt"></span> Gérer les services</a></li>
							<li><a href="?addRows="><span class="glyphicon glyphicon-plus"></span> Ajouter une personne</a></li>
							<li><a href="?addMairie="><span class="glyphicon glyphicon-plus"></span> Ajouter une Mairie</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="?logout="><span class="glyphicon glyphicon-log-out"></span> Se déconnecter</a></li>
						</ul>
					</div>

<?php
}
else // Déconnecté
{
?>

			<li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown"><span class="glyphicon glyphicon-log-in"></span> Login</a>
					<div class="dropdown-menu" style="padding: 15px; padding-bottom: 10px;">
						<form class="form-horizontal"  method="post" accept-charset="UTF-8" action="">
							<input id="sp_uname" class="form-control login" maxlength="15" type="text" name="username" placeholder="Nom d'utilisateur.." />
							<input id="sp_pass" class="form-control login" maxlength="15" type="password" name="password" placeholder="Mot de passe.."/>
							<input class="btn btn-primary" type="submit" name="submit" value="Se connecter" />
						</form>
					</div>
			</li>

<?php
}

?>

				</ul>
			</div>
		</nav>
  
		<div id="corpstxt">		
		<br/>
			<hr size="1" width="50%" color="white">				
				<h3><snap class="glyphicon glyphicon-earphone"></span>&nbsp; ANNUAIRE &nbsp;<span class="glyphicon glyphicon-earphone"></span></h3>
			<hr size="1" width="50%" color="white">
			
<?php
	if (isset($_GET['categ'])) // Affichage du bouton de recherche pour les mairies
	{
		if ($_GET['categ'] == "MAIRIE")
		{
			addViewOnUrl("Mairie CC");
			afficherRechercheMairie();
		}else
		{
			addViewOnUrl("Service : " . $_GET['categ']);
		}
	}else if (isset($_GET['search_mairie']))
	{
			afficherRechercheMairie();
	}

							echo "<br/>";

	if (isAdmin())
	{
						echo "<form method\"=get\" action=\"ChangesData.php\">";
	}

							echo "<table class=\"table-bordered\">";

	if (isset($_GET['categ'])) // Change de services
	{
		navigationCategories();
	}else if (isset($_GET['search'])) // Lance une recherche
	{
		rechercheBasique();
	}else if (isset($_GET['search_mairie'])) // Lance une recherche pour les mairies
	{
		rechercheMairie();
	}
	else if (isset($_GET['login'])) // Tentative de connexion
	{
		if ($_GET['login'] == "failed") // Tentative de connexion échouée
		{	
			echo "Identifiants incorrects. Veuillez réessayez ! <br/> 
			Vous allez être redirigé vers la page d'accueil dans 3 secondes !
			<meta http-equiv=\"refresh\" content=\"3; URL=Home.php\">";
		}
	}
	else if (isset($_GET['logout'])) // Déconnexion du client
	{
		if (isset($_SESSION['login']) && isset($_SESSION['password']))
		{
			deconnexionClient();
		}
	}
	else if (isset($_GET['addRows']))
	{
		if (isAdmin())
		{
			afficherAddData();
		}
	}else if (isset($_GET['addMairie']))
	{
		if (isAdmin())
		{
			afficherAddMairie();
		}
	}
	else if (isset($_GET['gererService']))
	{
		if (isAdmin())
		{
			afficherGererService();
		}
	}
	else if (isset($_GET['logs_connexions']))
	{
		if (isAdmin())
		{
			afficherTableauLogsConnexions();
		}
	}
	else if (isset($_GET['logs_changements']))
	{
		if (isAdmin())
		{
			afficherTableauLogsChangements();
		}
	}
	else if (isset($_GET['compteurs']))
	{
		if (isAdmin())
		{
			afficherTableauCompteurs();
		}
	}
	else if (isset($_GET['change'])) // Affichage du résultat des changements
	{
		if (isAdmin())
		{
			if ($_GET['change'] == "success" && isset($_GET['log']))
			{
				changementDataSuccess();
			}else if ($_GET['change'] == "failed")
			{
				changementDataFailed();
			}
		}
	}
	else // Page d'accueil
	{
		pageHome();
	}

	function tentativeConnexion($username, $password)
	{
		$username = $_POST['username'];
		$password = $_POST['password'];

		if (strlen($username) > 15 || strlen($password) > 15 || strlen($username) < 5 || strlen($password) <  5) // Mot de passe compris entre 5 et 15 lettres
		{
			return;
		}

		if (connexion($username, $password)) // Connexion réussie
		{
			ajouterLogConnexion($username, $password, "REUSSI");
			$_SESSION['login'] = $username;
			$_SESSION['password'] = $password;
			redirect('Home.php');
		}
		else // Connexion échoué
		{
			ajouterLogConnexion($username, $password, "ECHOUE");
			redirect('Home.php?login=failed');
		}
	}

	function afficherCategories()
	{
		global $mysqli;

		$result_1 = $mysqli->query("SELECT * FROM categorie");

		$num_categorie = $result_1->num_rows;

		while ($row_1 = $result_1->fetch_assoc())
		{
			$id_categorie = $row_1['id'];
			$nom_categorie = $row_1['nom'];

			$result_2 = $mysqli->query("SELECT * FROM service WHERE categorie_id = $id_categorie");
?>


				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo $nom_categorie;?>
					<span class="caret"></span></a>
						<ul class="dropdown-menu">

<?php
			while ($row_2 = $result_2->fetch_assoc())
			{
				$nom_service = $row_2['nom'];

				echo "<li><a href=\"?categ=$nom_service\">$nom_service</a></li>";
			}
?>

						</ul>
				</li>

<?php
		}
		$result = $mysqli->query("SELECT * FROM categorie, service");
	}

	function getnom_byserviceid($service_id, $mysqli)
	{
		global $mysqli;

		$result = $mysqli->query("SELECT * FROM service WHERE id=$service_id");

		if ($result->num_rows > 0)
		{	
			$row = $result->fetch_assoc();
			$service_nom = $row['nom'];

			return $service_nom;
		}
		$mysqli->close();
		return "-";
	}

	function isAdmin()
	 {
		 return (isset($_SESSION['login']) && isset($_SESSION['password']));
	 }

	function connexion($username, $password)
	{
		global $mysqli;

		$username = mysqli_real_escape_string($mysqli, addcslashes($username, "%_"));
		$password = mysqli_real_escape_string($mysqli, addcslashes($password, "%_"));

		$result = $mysqli->query("SELECT * FROM comptes WHERE nom_utilisateur='$username' And mot_de_passe='$password'");

		if ($result->num_rows > 0)
		{
			return true;
		}

		return false;
	}

	function ajouterLogChangement($log)
	{
		global $mysqli;

		$log = getCleanStr($log, $mysqli);

		$log_split = explode('<br/>', $log);

		for ($i = 0; $i < count($log_split) - 1; $i++)
		{
			$log_item = $log_split[$i];
			$username = getCleanStr($_SESSION['login'], $mysqli);
			$date = getDateNow();
			
			$query = "INSERT INTO logs_changements (username, date, changement) VALUES ('$username', '$date', '$log_item')";

			$mysqli->query($query);
		}
	}

	function getDateNow()
	{
		date_default_timezone_set('Europe/Paris');

		$date = (date("d-m-Y") . " - " . date("H:i"));

		return $date;
	}

	function ajouterLogConnexion($username, $password, $status)
	{
		global $mysqli;

		$username = getCleanStr($username, $mysqli);
		$password = getCleanStr($password, $mysqli);

		$date = getDateNow();

		$ip = getIp();
	
		$query = "INSERT INTO logs_connexions (ip, date, username, mot_de_passe, connexion) VALUES ('$ip', '$date', '$username', '$password', '$status')";
		$mysqli->query($query);
	}

	function navigationCategories()
	{
		global $mysqli;

		$categ = getCleanStr($_GET['categ'], $mysqli);

		if ($mysqli->connect_error)
		{
			die('Erreur');
		}

		$result = $mysqli->query("SELECT * FROM service, annuaire_data WHERE service.nom = '$categ' And service.id = annuaire_data.service_id");

		echo "
			<tr>
				<td id=\"titre\" colspan=\"2\">$categ</td>
			</tr>";

		if ($_GET['categ'] == "MAIRIE")
		{	
			ajusterTableauPourMairie();
			afficherMairie();
			return;
		}

		while($row = $result->fetch_assoc())
		{
			$id = $row['id'];
			$nom = $row['nom'];
			$numero = $row['numero'];

			if (isAdmin()) // Panel d'administration
			{
				echo "
				<tr>
					<td><input name=\"id_$id\" id=\"inputAdmin\" type=\"text\" value=\"$nom\"></td>
					<td><input name=\"id_" . $id . "_" . $nom . "_" . $numero . "\" id=\"inputAdmin\" type=\"text\" value=\"$numero\"></td>
					<td><a href=\"#\" onclick=\"deleteData('$id', '$nom', '$numero');\"><span class=\"glyphicon glyphicon-remove\"></span></a></td>
				</tr>";
			}
			else // Affichage normal
			{
				echo "
				<tr>
					<td>$nom</td>
					<td>$numero</td>
				</tr>";
			}
			
		}
		AfficherBoutonConfirmation();
	}

	function afficherAddData()
	{
		global $mysqli;
?>
	<h3 style="color:gold;"> Ajouter une personne dans la catégorie que vous souhaitez </h3>

	<br/>
	<br/>

	<form method="get" action="ChangesData.php">
			<div align="center" class="test">
				<div class="col-lg-6">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1">Nom</span>
						<input name="ajouter_nom" type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
					</div>
				</div>
				<div class="col-lg-6">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1">N°tel</span>
						<input name="ajouter_numero" type="text" class="form-control" placeholder="Phone number" aria-describedby="basic-addon1">
					</div>
				</div>
			</div>
		
			<br/>
			<br/>
			<br/>
			
			<div class="btn-group">
				<select name="service" class="form-control">
	<?php

				$result_1 = $mysqli->query("SELECT * FROM categorie");

				while ($row_1 = $result_1->fetch_assoc())
				{
					$categorie_nom = $row_1['nom'];
					$categorie_id = $row_1['id'];

					echo "<optgroup label=\"$categorie_nom\">";

					$result_2 = $mysqli->query("SELECT * FROM service WHERE categorie_id=$categorie_id");

					while ($row_2 = $result_2->fetch_assoc())
					{
						$nom_service = $row_2['nom'];

						echo "<option value=\"$nom_service\">$nom_service</option>";
					}

					echo "</optgroup>";
				}

	?>
				</select>
	
			</div>
			
			<div class="btn-group">
				<button type="submit" class="btn btn-danger">Ajouter <span class="glyphicon glyphicon-plus-sign"></span></button>
			</div>
	</form>
			<br/>
			<br/>
<?php
	}

	function afficherAddMairie()
	{
		global $mysqli;
/*		}else if (isset($_GET['nom_mairie']) && isset($_GET['nom_maire']) && isset($_GET['adresse_perso']) && isset($_GET['portable_maire']) 
			   && isset($_GET['tel_mairie']) && isset($_GET['e-mail']) && isset($_GET['adresse_mairie']))*/
?>
	<form method="get" action="ChangesData.php">
		<h3 style="color:gold;"> Ajouter une Mairie </h3>
		<div class="col-lg-4 col-lg-offset-4">
			<div class="input-group" >
				<span class="input-group-addon" id="basic-addon1">Mairie</span>
				<input name="nom_mairie" type="text" class="form-control" placeholder="Vitry-le-François" aria-describedby="basic-addon1">
			</div>
		</div>
		
		<br/>
		<br/>
		
		<div class="col-lg-4 col-lg-offset-4">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Nom Maire</span>
				<input name="nom_maire" ctype="text" class="form-control" placeholder="Josh Smith" aria-describedby="basic-addon1">
			</div>
		</div>
		
		<br/>
		<br/>
		
		<div class="col-lg-4 col-lg-offset-4">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Adresse perso</span>
				<input name="adresse_perso" type="text" class="form-control" placeholder="17 rue de l'Arquebuse" aria-describedby="basic-addon1">
			</div>
		</div>
		
		<br/>
		<br/>
		
		<div class="col-lg-4 col-lg-offset-4">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Portables Maire</span>
				<input name="portable_maire" type="text" class="form-control" placeholder="xx.xx.xx.xx.xx / yy.yy.yy.yy.yy" aria-describedby="basic-addon1">
			</div>
		</div>
		
		<br/>
		<br/>
		
		<div class="col-lg-4 col-lg-offset-4">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Tel Mairie</span>
				<input name="tel_mairie" type="text" class="form-control" placeholder="03.26.72.55.14" aria-describedby="basic-addon1">
			</div>
		</div>
		
		<br/>
		<br/>
		
		<div class="col-lg-4 col-lg-offset-4">
			<div class="input-group">
				<input name="e-mail" type="text" class="form-control" placeholder="jsmith" aria-describedby="basic-addon2">
				<span class="input-group-addon" id="basic-addon2">@example.com</span>
			</div>
		</div>
		
		<br/>
		<br/>
		
		<div class="col-lg-4 col-lg-offset-4">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Adresse Mairie</span>
				<input name="adresse_mairie" type="text" class="form-control" placeholder="5 rue Marcel Bailly" aria-describedby="basic-addon1">
			</div>
		</div>
		
		<br/>
		<br/>
	
		<div class="btn-group">
			<button type="submit" class="btn btn-primary" aria-haspopup="true" aria-expanded="false">Ajouter <span class="glyphicon glyphicon-plus"></span></button>
		</div>
	</form>

<?php

	}

	function afficherGererService()
	{
		global $mysqli;
?>
		<h3 style="color:gold;"> Ajouter un service dans une des catégories que vous souhaitez </h3>

	<br/>
	<br/>

	<form method="get" action="ChangesData.php">
				<div class="col-lg-4 col-lg-offset-4">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1">Nom</span>
						<input name="nom_service" type="text" class="form-control" placeholder="Nom du service" aria-describedby="basic-addon1">
					</div>
				</div>

			<br/>
			<br/>
			<br/>
			
			<div class="btn-group">
					<select name="nom_categorie" class=form-control>
						<optgroup label="Catégories">
<?php
		

		$result = $mysqli->query("SELECT * FROM categorie");

		while ($row = $result->fetch_assoc())
		{
			$nom = $row['nom'];

			echo "<option value=\"$nom\">$nom</option>";
		}
				
?>
					</optgroup>
				</select>

			</div>
			
			<div class="btn-group">
				<button type="submit" class="btn btn-primary">Ajouter <span class="glyphicon glyphicon-plus-sign"></span></button>
			</div>
	</form>

	<br/>
	<br/>

	<hr width="70%">

	<br/>

	<h3 style="color:gold;"> Supprimer un service </h3>

	<br/>
	<form method="get" action="ChangesData.php">
		<div class="btn-group">
			<select name="delete_service" class="form-control">
<?php

			$result_1 = $mysqli->query("SELECT * FROM categorie");

			while ($row_1 = $result_1->fetch_assoc())
			{
				$categorie_nom = $row_1['nom'];
				$categorie_id = $row_1['id'];

				echo "<optgroup label=\"$categorie_nom\">";

				$result_2 = $mysqli->query("SELECT * FROM service WHERE categorie_id=$categorie_id");

				while ($row_2 = $result_2->fetch_assoc())
				{
					$nom_service = $row_2['nom'];

					echo "<option value=\"$nom_service\">$nom_service</option>";
				}

				echo "</optgroup>";
			}

?>
			</select>
	
		</div>
		<div class="btn-group">
			<button type="submit" class="btn btn-danger">Supprimer <span class="glyphicon glyphicon-plus-sign"></span></button>
		</div>
	</form>

<?php
	}

	function rechercheBasique()
	{
		global $mysqli;

		echo "<script>
				document.getElementsByName(\"search\")[0].value = \"" . $_GET['search'] . "\";
				</script>";

		$search = mysqli_real_escape_string($mysqli, addcslashes($_GET['search'], "%_"));

		$result_1 = $mysqli->query("SELECT * FROM service");

		$numRowsFromservice = $result_1->num_rows;

		while ($row_1 = $result_1->fetch_assoc())
		{
			$id = $row_1['id'];
		
			$result_2 = $mysqli->query("SELECT * FROM annuaire_data WHERE LOWER(nom) LIKE LOWER('%$search%') And service_id=$id");
	
			if ($result_2->num_rows > 0)
			{
				$service_nom = getnom_byserviceid($id, $mysqli);

				if ($service_nom != "-")
				{
					echo "
						<tr>
							<td id=\"titre\" colspan=\"2\">$service_nom</td>
						</tr>";
					while ($row = $result_2->fetch_assoc())
					{
						$id = $row['id'];
						$nom = $row['nom'];
						$numero = $row['numero'];

						if (isAdmin()) // Panel d'administration
						{
							echo "
							<tr>
								<td><input name=\"id_$id\" id=\"inputAdmin\" type=\"text\" value=\"$nom\"></td>
								<td><input name=\"id_" . $id . "_" . $nom . "_" . $numero . "\" id=\"inputAdmin\" type=\"text\" value=\"$numero\"></td>
								<td><a href=\"#\" onclick=\"deleteData('$id', '$nom', '$numero');\"><span class=\"glyphicon glyphicon-remove\"></span></a></td>
							</tr>";
						}
						else // Affichage normal
						{
							echo "
							<tr>
								<td>$nom</td>
								<td>$numero</td>
							</tr>";
						}
					}
				}
			}
		}
		AfficherBoutonConfirmation();
	}

	function rechercheMairie()
	{
		global $mysqli;
		echo "<script>
				document.getElementsByName(\"search_mairie\")[0].value = \"" . $_GET['search_mairie'] . "\";
				</script>";

		

		$search_mairie = mysqli_real_escape_string($mysqli, addcslashes($_GET['search_mairie'], "%_"));

		if ($mysqli->connect_error)
		{
			die('Erreur');
		}

		$result = $mysqli->query("SELECT * FROM mairie WHERE 
		LOWER(nom_mairie) LIKE LOWER('%$search_mairie%') 
		OR LOWER(nom) LIKE LOWER('%$search_mairie%') 
		OR LOWER(adresse_perso) LIKE LOWER('%$search_mairie%') 
		OR LOWER(portable_maire) LIKE LOWER('%$search_mairie%') 
		OR LOWER(tel_mairie) LIKE LOWER('%$$search_mairie%') 
		OR LOWER(e_mail) LIKE LOWER('%$search_mairie%') 
		OR LOWER(adresse_mairie) LIKE LOWER('%$search_mairie%')");

		ajusterTableauPourMairie();

		$count = 0;
		while ($row = $result->fetch_assoc())
		{
			$id = $row['id'];
			$nom_mairie = $row['nom_mairie'];
			$nom = $row['nom'];
			$adresse_perso = $row['adresse_perso'];
			$portable_maire = $row['portable_maire'];
			$tel_mairie = $row['tel_mairie'];
			$e_mail = $row['e_mail'];
			$adresse_mairie = $row['adresse_mairie'];

			if (isAdmin()) // Panel d'administration
			{
						$name_nom_mairie = "id_$id";
					echo "
					<tr>";
						echo "<td><input name=\"$name_nom_mairie\" id=\"inputAdmin\" type=\"text\" value=\"$nom_mairie\"></td>";
						echo "<td><input name=\"0x$count\" id=\"inputAdmin\" type=\"text\" value=\"$nom\"></td>";
						$count++;
						echo "<td><input name=\"0x$count\" id=\"inputAdmin\" type=\"text\" value=\"$adresse_perso\"></td>";
						$count++;
						echo "<td><input name=\"0x$count\" id=\"inputAdmin\" type=\"text\" value=\"$portable_maire\"></td>";
						$count++;
						echo "<td><input name=\"0x$count\" id=\"inputAdmin\" type=\"text\" value=\"$tel_mairie\"></td>";
						$count++;
						echo "<td><input name=\"0x$count\" id=\"inputAdmin\" type=\"text\" value=\"$e_mail\"></td>";
						$count++;
						echo "<td><input name=\"0x$count\" id=\"inputAdmin\" type=\"text\" value=\"$adresse_mairie\"></td>
						<td><a href=\"#\" onclick=\"deleteDataMairie('$id', '" . getCleanStr($nom_mairie, $mysqli) . "');\"><span class=\"glyphicon glyphicon-remove\"></span></a></td>
					</tr>";
					$count++;
			}else // Affichage normale
			{
				echo "
				<tr>
					<td>$nom_mairie</td>
					<td>$nom</td>
					<td>$adresse_perso</td>
					<td>$portable_maire</td>
					<td>$tel_mairie</td>
					<td>$e_mail</td>
					<td>$adresse_mairie</td>
				</tr>";
			}
		}
		AfficherBoutonConfirmation();
		ajusterCellulePourMairie();
	}

	function deconnexionClient()
	{
		session_unset();

		session_destroy();
		
		redirect('Home.php');
	}

	function changementDataSuccess()
	{
?>

		<div class="alert alert-success alert-dismissible" role="alert">
			<strong>Success!</strong> Les changements ont été effectués avec succès !
			<br/>
			--------- LOG ---------
			<br/>
<?php

	$log = $_GET['log'];

	ajouterLogChangement($_GET['log']);

	echo $log;

?>
			<br/>
			Vous allez être redirigé vers la page d'accueil dans 10 secondes !
		</div>
		<meta http-equiv="refresh" content="10; URL=Home.php">

<?php
	}

	function changementDataFailed()
	{
?>		

		<div class="alert alert-danger alert-dismissible" role="alert">
			<strong>Erreur!</strong> Une erreur est survenue lors de l'opération ! Veuillez réessayez !
			<br/>
			Vous allez être redirigé vers la page d'accueil
		</div>
		<meta http-equiv="refresh" content="3; URL=Home.php">

<?php
	}

	function pageHome()
	{

?>

			<div>
				<p align="justify;center" >
			
					Bienvenue sur l'annuaire en ligne regroupant tous les numéros appartenant aux services et personnes de la Communauté de Communes de Vitry-le-François.
					Si vous recherchez une personne ou un service, utilisez la barre de recherche ci-dessus. Vous pouvez aussi les retrouver via la barre menu.
		
					<br/>
					<br/>
			
					Merci et bonne utilisation.
			
				</p>
			</div>

			<br/>
			<br/>

			<div class="divboite">
				<div class="fb-like-box fb_iframe_widget" href="https://www.facebook.com/vitrylefrancois" fb-iframe-plugin-query="app_id=&amp;href=https%3A%2F%2Fwww.facebook.com%2Fvitrylefrancois&amp;locale=en_US&amp;sdk=joey&amp;show_border=true&amp;show_faces=false&amp;stream=true&amp;width=500"><span style="vertical-align: bottom"><iframe name="ff4e765e7f711c" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:like_box Facebook Social Plugin" src="https://www.facebook.com/plugins/like_box.php?app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2F0F7S7QWJ0Ac.js%3Fversion%3D42%23cb%3Df32a7bc9763c58%26domain%3Dwww.vitry-le-francois.net%26origin%3Dhttps%253A%252F%252Fwww.vitry-le-francois.net%252Ff2d3cc231c6851c%26relation%3Dparent.parent&amp;color_scheme=light&amp;container_width=500&amp;header=false&amp;height=850&amp;href=https%3A%2F%2Fwww.facebook.com%2Fvitrylefrancois&amp;locale=en_US&amp;sdk=joey&amp;show_border=true&amp;show_faces=false&amp;stream=true&amp;width=500" style="border: none; visibility: visible; width: 500px; height: 850px;" class=""></iframe></span></div>
			</div>

			<div style="text-align:center;width:400px;padding:1em 0;margin:0 auto;">
				<h2>
					<span style="color:gold;">Heure actuelle</span><br />Vitry-le-François, France
				</h2> 
				<iframe src="https://www.zeitverschiebung.net/clock-widget-iframe-v2?language=fr&timezone=Europe%2FParis" width="100%" height="150" frameborder="0" seamless></iframe>
			</div>

<?php

	}

	function afficherRechercheMairie()
	{
?>

		<form class="navbar-form navbar-center" method="get" action="">
					<div class="input-group">
						<input type="text" name="search_mairie" class="form-control" placeholder="Rechercher dans les Mairies">
							<div class="input-group-btn">
								<button class="btn btn-default" type="submit">
									<i class="glyphicon glyphicon-search"></i>
								</button>
							</div>
					</div>
			</form>

<?php
	}

	function AfficherBoutonConfirmation()
	{
		if (isAdmin())
		{
?>

			<div class="btn-group">
				<button id="confirmation" type="submit" class="btn btn-primary" aria-haspopup="true" aria-expanded="false" href="#">Appliquer les modifications <span class="glyphicon glyphicon-check"></button>
			</div>

<?php
		}
	}

	function AfficherBoutonSupprimer()
	{
		if (isAdmin())
		{
?>

			<div class="btn-group">
				<button id="supprimer" type="button" class="btn btn-danger" aria-haspopup="true" aria-expanded="false" onclick="CreateLink()">Tout supprimer <span class="glyphicon glyphicon-trash"></button>
			</div>

<?php
		}
	}

	function AfficherBoutonTelecharger()
	{
		if (isAdmin())
		{
?>

			<div class="btn-group">
				<button id="telecharger" type="button" class="btn btn-success" aria-haspopup="true" aria-expanded="false" onclick="CreateLinkDownload()">Télécharger <span class="glyphicon glyphicon-download-alt"></button>
			</div>

<?php
		}
	}

	function afficherMairie()
	{
		global $mysqli;

		$result = $mysqli->query("SELECT * FROM mairie");
		
		$count = 0;
		while ($row = $result->fetch_assoc())
		{
			$id = $row['id'];
			$nom_mairie = $row['nom_mairie'];
			$nom = $row['nom'];
			$adresse_perso = $row['adresse_perso'];
			$portable_maire = $row['portable_maire'];
			$tel_mairie = $row['tel_mairie'];
			$e_mail = $row['e_mail'];
			$adresse_mairie = $row['adresse_mairie'];

			if (isAdmin()) // Panel d'administration
			{
					$name_nom_mairie = "id_$id";
					echo "
					<tr>";
						echo "<td><input name=\"$name_nom_mairie\" id=\"inputAdmin\" type=\"text\" value=\"$nom_mairie\"></td>";
						echo "<td><input name=\"0x$count\" id=\"inputAdmin\" type=\"text\" value=\"$nom\"></td>";
						$count++;
						echo "<td><input name=\"0x$count\" id=\"inputAdmin\" type=\"text\" value=\"$adresse_perso\"></td>";
						$count++;
						echo "<td><input name=\"0x$count\" id=\"inputAdmin\" type=\"text\" value=\"$portable_maire\"></td>";
						$count++;
						echo "<td><input name=\"0x$count\" id=\"inputAdmin\" type=\"text\" value=\"$tel_mairie\"></td>";
						$count++;
						echo "<td><input name=\"0x$count\" id=\"inputAdmin\" type=\"text\" value=\"$e_mail\"></td>";
						$count++;
						echo "<td><input name=\"0x$count\" id=\"inputAdmin\" type=\"text\" value=\"$adresse_mairie\"></td>
						<td><a href=\"#\" onclick=\"deleteDataMairie('$id', '" . getCleanStr($nom_mairie, $mysqli) . "');\"><span class=\"glyphicon glyphicon-remove\"></span></a></td>
					</tr>";
					$count++;
			}else // Affichage normale
			{
				echo "
				<tr>
					<td>$nom_mairie</td>
					<td>$nom</td>
					<td>$adresse_perso</td>
					<td>$portable_maire</td>
					<td>$tel_mairie</td>
					<td>$e_mail</td>
					<td>$adresse_mairie</td>
				</tr>";
			}
		}
		AfficherBoutonConfirmation();
		ajusterCellulePourMairie();
	}

	function afficherTableauCompteurs()
	{
		global $mysqli;

		if ($mysqli->connect_error)
		{
			return;
		}
	
?>
			<script>
					var elmt = document.getElementById("corpstxt");
					elmt.style.marginLeft = "20px";
					elmt.style.marginRight = "20px";
		
					document.getElementById("titre").colSpan = "7";

					document.getElementById("tbl_data").style.width = "97%";

					function CreateLink()
					{
						if (confirm("Êtes-vous sûre de vouloir réinitialiser les compteurs de vues ?"))
						{
							location.href = 'ChangesData.php?DeleteLogsCompteurs=';
						}
					}
					
			</script>
			<tr>
				<td id="titre" colspan="2">COMPTEURS</td>
			</tr>

			<tr>
				<td style="color:gold">URL</td>
				<td style="color:gold">VUES</td>
			</tr>
<?php

		$result = $mysqli->query("SELECT * FROM compteurs");

		while($row = $result->fetch_assoc())
		{
			$url = $row['url'];
			$vues = $row['vues'];

			echo "
			<tr>
				<td>$url</td>
				<td>$vues</td>
			</tr>";
		}
		AfficherBoutonSupprimer();
	}

	function afficherTableauLogsChangements()
	{

		global $mysqli;

		if ($mysqli->connect_error)
		{
			return;
		}
?>
			<script>
					var elmt = document.getElementById("corpstxt");
					elmt.style.marginLeft = "20px";
					elmt.style.marginRight = "20px";
		
					document.getElementById("titre").colSpan = "7";

					document.getElementById("tbl_data").style.width = "97%";

					function CreateLink()
					{
						if (confirm("Êtes-vous sûre de vouloir supprimer tous les logs de changements ?"))
						{
							location.href = 'ChangesData.php?DeleteLogsChangements=';
						}
					}

					function CreateLinkDownload()
					{
						window.open("SaveLog.php?changements=");
					}
			</script>
			<tr>
				<td id="titre" colspan="3">LOGS DES CHANGEMENTS</td>
			</tr>

			<tr>
				<td style="color:gold">CHANGEMENTS</td>
				<td style="color:gold">DATE</td>
				<td style="color:gold">UTILISATEUR</td>
			</tr>
<?php

		$result = $mysqli->query("SELECT * FROM logs_changements");

		while($row = $result->fetch_assoc())
		{
			$username = $row['username'];
			$date = $row['date'];
			$changement = $row['changement'];

			echo "
			<tr>
				<td>$changement</td>
				<td>$date</td>
				<td>$username</td>
			</tr>";
		}
		AfficherBoutonSupprimer();
		AfficherBoutonTelecharger();
	}

	function afficherTableauLogsConnexions()
	{
		global $mysqli;

		if ($mysqli->connect_error)
		{
			return;
		}
?>
			<script>
					var elmt = document.getElementById("corpstxt");
					elmt.style.marginLeft = "20px";
					elmt.style.marginRight = "20px";

					document.getElementById("titre").colSpan = "7";

					document.getElementById("tbl_data").style.width = "97%";

					function CreateLink()
					{
						if (confirm("Êtes-vous sûre de vouloir supprimer tous les logs de connexions ?"))
						{
							location.href = 'ChangesData.php?DeleteLogsConnexions=';
						}
					}

					function CreateLinkDownload()
					{
						window.open("SaveLog.php?connexions=");
					}
			</script>
			<tr>
				<td id="titre" colspan="5">LOGS DES CONNEXIONS</td>
			</tr>

			<tr>
				<td style="color:gold">IP</td>
				<td style="color:gold">DATE</td>
				<td style="color:gold">UTILISATEUR</td>
				<td style="color:gold">MOT DE PASSE</td>
				<td style="color:gold">CONNEXION</td>
			</tr>
<?php

		$result = $mysqli->query("SELECT * FROM logs_connexions");

		while($row = $result->fetch_assoc())
		{
			$ip = $row['ip'];
			$date = $row['date'];
			$username = $row['username'];
			$mdp = $row['mot_de_passe'];
			$connexion = $row['connexion'];

			echo "
			<tr>
				<td>$ip</td>
				<td>$date</td>
				<td>$username</td>
				<td>$mdp</td>
				<td>$connexion</td>
			</tr>";
		}
		AfficherBoutonSupprimer();
		AfficherBoutonTelecharger();
	}

	function ajusterTableauPourMairie()
	{
?>
			<tr>
				<td style="color:gold">MAIRIES</td>
				<td style="color:gold">NOM /MAIRES</td>
				<td style="color:gold">Adresses perso</td>
				<td style="color:gold">Portables maires</td>
				<td style="color:gold">tel mairie</td>
				<td style="color:gold">e-mails</td>
				<td style="color:gold">Adresse Mairies</td>
			</tr>

				<script>
					var elmt = document.getElementById("corpstxt");
					elmt.style.marginLeft = "20px";
					elmt.style.marginRight = "20px";

					document.getElementById("titre").colSpan = "7";

					document.getElementsByTagName("table")[0].style.width = "97%";
				</script>
<?php
	}

	function ajusterCellulePourMairie()
	{
?>

		<script>
			var tds = document.getElementsByTagName("td");

			for (var i = 1; i < tds.length; i++)
			{
				tds[i].style.fontSize = "17px";
			}
		</script>

<?php
	}

	function getCleanStr($str, $mysqli)
	{
		$str =  mysqli_real_escape_string($mysqli, addcslashes($str, "%_"));
		return $str;
	}

	function getIp()
	{
		if (isset($_SERVER['HTTP_CLIENT_IP'])) 
		{
			return $_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) 
		{
			return $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else 
		{
			return (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');
		}
	}

	function addViewOnUrl($url)
	{
		global $mysqli;

		$url = getCleanStr($url, $mysqli);

		$result = $mysqli->query("SELECT * FROM compteurs WHERE url='$url'");

		if ($result->num_rows > 0) // l'URL existe déjà
		{
			$row = $result->fetch_assoc();
			$vue = $row['vues'];
			$query = "UPDATE compteurs SET vues='" . ($vue + 1) . "' WHERE url='$url'";
			$mysqli->query($query);
		}
		else
		{
			$query = "INSERT INTO compteurs (url, vues) VALUES ('$url', '1')";
			$mysqli->query($query);
		}

		return false;
	}

	function redirect($url)
	{
		echo "<script>
					window.location = '$url';
				 </script>";
	}	

?>

			</table>
<?php

	if (isAdmin())
	{
		echo "</form>";
	}

?>

			<br/>

			<br/>
			<br/>
			<br/>

		</div>

	</body>
</html>